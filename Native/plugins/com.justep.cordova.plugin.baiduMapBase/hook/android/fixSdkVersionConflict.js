#!/usr/bin/env node
var fs = require('fs'),
    path = require('path'),
    rootdir = process.argv[2],
    events = new (require('events').EventEmitter)(),
    shell = require('shelljs');

if (!rootdir)
    return;

module.exports = function (context) {
	var cordova_util = context.requireCordovaModule('cordova-lib/src/cordova/util'),
        ConfigParser = context.requireCordovaModule('cordova-lib/src/configparser/ConfigParser'),
        platforms = context.requireCordovaModule('cordova-lib/src/cordova/platform'),
        projectRoot = cordova_util.isCordova();
    var xml = path.join(projectRoot, 'config.xml');
    var cfg = new ConfigParser(xml);
    var packageName = cfg.packageName();

    /**
     * The absolute path for the file.
     *
     * @param {String} platform
     *      The name of the platform like 'ios'.
     * @param {String} relPath
     *      The relative path from the platform root directory.
     *
     * @return String
     */
    function getAbsPath(platform, relPath) {
        var platform_path = path.join(projectRoot, 'platforms', platform);
        console.log(platform_path);
        return path.join(platform_path, relPath);
    }
    
    function replace (path, to_replace, replace_with) {
        var data = fs.readFileSync(path, 'utf8'),
            result;
        if (data.indexOf(replace_with) > -1)
            return;
        result = data.replace(to_replace, replace_with);
        fs.writeFileSync(path, result, 'utf8');
    }
    
    
    var armlocSDK5Path = getAbsPath('android', '/libs/armeabi/liblocSDK5.so');
    var armv7alocSDK5Path = getAbsPath('android', '/libs/armeabi-v7a/liblocSDK5.so');
    var mipslocSDK5Path = getAbsPath('android', '/libs/mips/liblocSDK5.so');
    var mips64locSDK5Path = getAbsPath('android', '/libs/mips64/liblocSDK5.so');
    var x86locSDK5Path = getAbsPath('android', '/libs/x86/liblocSDK5.so');
    var x8664locSDK5Path = getAbsPath('android', '/libs/x86_64/liblocSDK5.so');
    var locSDK5Path = getAbsPath('android', '/libs/locSDK_5.01.jar');
    
    var manifestPath = getAbsPath('android', '/AndroidManifest.xml');
    
    if(context.hook == "before_plugin_install"){
    	shell.rm('-rf', armlocSDK5Path);
    	shell.rm('-rf', armv7alocSDK5Path);
    	shell.rm('-rf', mipslocSDK5Path);
    	shell.rm('-rf', mips64locSDK5Path);
    	shell.rm('-rf', x86locSDK5Path);
    	shell.rm('-rf', x8664locSDK5Path);
    	shell.rm('-rf', locSDK5Path);
    }else if(context.hook == "after_plugin_rm"){
    	//TODO 还原获取
    }else if(context.hook == "after_prepare"){
    	replace(manifestPath,"com.baidu.lbsapi.API_KEY","com.baidu.lbsapi.API_KEY_badulocation_bak");
    }
};