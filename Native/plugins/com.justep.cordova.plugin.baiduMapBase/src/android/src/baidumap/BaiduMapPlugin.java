package baidumap;


import java.util.ArrayList;
import java.util.List;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.ArcOptions;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.CircleOptions;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.Overlay;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.PolygonOptions;
import com.baidu.mapapi.map.Polyline;
import com.baidu.mapapi.map.PolylineOptions;
import com.baidu.mapapi.map.Stroke;
import com.baidu.mapapi.map.UiSettings;
import com.baidu.mapapi.map.offline.MKOLSearchRecord;
import com.baidu.mapapi.map.offline.MKOLUpdateElement;
import com.baidu.mapapi.map.offline.MKOfflineMap;
import com.baidu.mapapi.map.offline.MKOfflineMapListener;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.LatLngBounds;
import com.baidu.mapapi.utils.CoordinateConverter;
import com.baidu.mapapi.utils.DistanceUtil;

import util.Http;


@SuppressLint("UseSparseArrays")
public class BaiduMapPlugin extends CordovaPlugin implements BaiduMap.OnMapClickListener, MKOfflineMapListener {


    public CallbackContext callbackContext;
    public static Activity activity;

    private static FakeR fakeR;
    /**
     * MapView 是地图主控件
     */
    public static MapView mMapView;
    public static BaiduMap mBaiduMap;

    static LinearLayout layout;
    /**
     * 当前地点击点
     */
    private LatLng currentPt;
    /**
     * 当前定位地址
     */
    public static LatLng currentLocation;

    /**
     * 当前定位地址
     */
    static BDLocation currentBDLoc;

    //离线地图
    static MKOfflineMap mOffline = null;
    private String touchType;
    private String listenResult;
    // 定位相关
    LocationClient mLocClient;
    public MyLocationListenner myListener = new MyLocationListenner();
    private static MyLocationConfiguration.LocationMode mCurrentMode;
    //定位图标
    static BitmapDescriptor mCurrentMarker;
    static boolean isFirstLoc = true; // 是否首次定位

    static List<Info> listOop = new ArrayList<Info>();
    static Overlay overlay = null;
    boolean loaded = false;
    // 浏览路线节点相关
    Button mBtnPre = null; // 上一个节点
    Button mBtnNext = null; // 下一个节点
    int nodeIndex = -1; // 节点索引,供浏览节点时使用
    //	static LatLng center = new LatLng(39.92235, 116.380338);
    static int radius = 500;
    LatLng southwest = new LatLng(39.92235, 116.380338);
    LatLng northeast = new LatLng(39.947246, 116.414977);
    LatLngBounds searchbound = new LatLngBounds.Builder().include(southwest).include(northeast).build();

    static int searchType = 0;  // 搜索的类型，在显示时区分
    /**
     * 用于显示地图状态的面板
     */
    private TextView mStateBar;
    //地图设置
    public static UiSettings mUiSettings;
    //Marker信息显示
    private InfoWindow mInfoWindow;
    static CallbackContext ccForOffine;

    @Override
    public boolean execute(String action, final JSONArray args,
                           final CallbackContext callbackContext) {
        setCallbackContext(callbackContext);
        fakeR = new FakeR(cordova.getActivity());
        SDKInitializer.initialize(cordova.getActivity().getApplicationContext());
        try {
            JSONObject jsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            //离线地图初始化
            offLineMapInit();

            if ("open".equals(action)) {
                cordova.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            init(args.getJSONObject(0), callbackContext);
                        } catch (JSONException js) {
                            js.printStackTrace();
                        }
                    }
                });


            } else if ("close".equals(action)) {
                cordova.getActivity().runOnUiThread(new Runnable() {
                    public void run() {


                        if (mMapView != null) {
                            layout.removeView(mMapView);
                            mMapView.onDestroy();
                            mMapView = null;
                        }
                        callbackContext.success(); // Thread-safe.
                    }
                });
            } else if ("setPosition".equals(action)) {//获取当前坐标
                JSONObject json = args.getJSONObject(0);
                setPosition(json);
            } else if ("getCurrentLocation".equals(action)) {//获取当前坐标
                String loc = getLocation();
                JSONObject resultJson = null;
                try{
                	resultJson =new JSONObject(loc);                    
                }catch (JSONException e) {
                	throw new RuntimeException(e);
                }
                callbackContext.success(resultJson);
            } else if ("getLocationFromName".equals(action)) {//根据地址信息获取经纬度
                JSONObject json = args.getJSONObject(0);
                String address = json.getString("address");
                String city = json.getString("city");
                getLocationFromName(address, city, callbackContext);
            } else if ("getNameFromLocation".equals(action)) {//根据经纬度获取地址信息
                JSONObject json = args.getJSONObject(0);
                String lon = json.getString("lon");
                String lat = json.getString("lat");
                getNameFromLocation(lat, lon, callbackContext);

            } else if ("showCurrentLocation".equals(action)) {//是否在地图上显示当前位置，并且设置显示的样式
                jsonObject = args.getJSONObject(0);
                showCurrentLocation(jsonObject.getBoolean("isShow"), jsonObject.getString("trackingMode"));
                callbackContext.success();
            } else if ("setCenter".equals(action)) {
                jsonObject = args.getJSONObject(0);
                setCenter(Double.valueOf(jsonObject.getString("lon")), Double.valueOf(jsonObject.getString("lat")));
//                callbackContext.success("");
            } else if ("getCenter".equals(action)) {
                String loc = getCenter();
                if (loc.contains("error")) {
                    callbackContext.success(loc);
                } else {
                    callbackContext.error(loc);
                }
            } else if ("setZoomLevel".equals(action)) {
                setZoomLevel(args.get(0).toString());
            } else if ("setMapAttr".equals(action)) {
                jsonObject = args.getJSONObject(0);
                String type = jsonObject.getString("type");
                Boolean zoomEnable = jsonObject.getBoolean("zoomEnable");
                Boolean scrollEnable = jsonObject.getBoolean("scrollEnable");
                setMapAttr(type, zoomEnable, scrollEnable);
            } else if ("setRotation".equals(action)) {
                String degrees = args.get(0).toString();
                setRotation(Float.parseFloat(degrees));
            } else if ("setOverlook".equals(action)) {
                String degrees = args.get(0).toString();
                setOverlook(Float.parseFloat(degrees));
            } else if ("setScaleBar".equals(action)) {
                jsonObject = args.getJSONObject(0);
                Boolean isShow = jsonObject.getBoolean("isShow");
                JSONObject po = jsonObject.getJSONObject("position");
                setScaleBar(isShow, po.getInt("x"), po.getInt("y"));
            } else if ("setCompass".equals(action)) {
                jsonObject = args.getJSONObject(0);
                setCompass(jsonObject.getInt("x"), jsonObject.getInt("y"));
            } else if ("setTraffic".equals(action)) {
                setTraffic(args.getBoolean(0));
            } else if ("setHeatMap".equals(action)) {
                setHeatMap(args.getBoolean(0));
            } else if ("setBuilding".equals(action)) {
                setBuilding(args.getBoolean(0));
            } else if ("setRegion".equals(action)) {
                JSONObject js = args.getJSONObject(0);
                setRegion(js);
            } else if ("getRegion".equals(action)) {
                JSONObject js = getRegion();
                callbackContext.success(js);
            } else if ("zoomIn".equals(action)) {
                zoomIn();
            } else if ("zoomOut".equals(action)) {
                zoomOut();
            } else if ("addAnnotations".equals(action)) {
                jsonArray = args.getJSONArray(0);
                addAnnotations(jsonArray);
            } else if ("removeAnnotations".equals(action)) {
                jsonArray = args.getJSONArray(0);
                removeAnnotations(jsonArray);
            } else if ("removeAllAnno".equals(action)) {
                removeAllAnno();
            } else if ("getAnnotationCoords".equals(action)) {
                String id = args.getString(0);
                LatLng latLng = getAnnotationCoords(id);
                String coords = "{lon:" + latLng.longitude + ",lat:" + latLng.latitude + "}";
                callbackContext.success(coords);
            } else if ("updateAnnotationCoords".equals(action)) {
                JSONObject ja = args.getJSONObject(0);
                updateAnnotationCoords(ja);
            } else if ("annotationExist".equals(action)) {
                String id = args.getString(0);
                Boolean result = annotationExist(id);
                if (result) {
                    callbackContext.success();
                } else {
                    callbackContext.error("");
                }
            } else if ("addLine".equals(action)) {
                JSONObject ja = args.getJSONObject(0);
                addLine(ja);
            } else if ("addPolygon".equals(action)) {
                JSONObject ja = args.getJSONObject(0);
                addPolygon(ja);
            } else if ("addArc".equals(action)) {
                JSONObject ja = args.getJSONObject(0);
                addArc(ja);
            } else if ("addCircle".equals(action)) {
                JSONObject ja = args.getJSONObject(0);
                addCircle(ja);
            } else if ("removeOverlay".equals(action)) {
                JSONArray ja = args.getJSONArray(0);
                removeOverlay(ja);
            } else if ("addOfflineListener".equals(action)) {
                ccForOffine = callbackContext;
            } else if ("getHotCityList".equals(action)) {
                JSONArray ja = getHotCityList();
                callbackContext.success(ja);
            } else if ("getOfflineCityList".equals(action)) {
                JSONArray ja = getOfflineCityList();
                callbackContext.success(ja);
            } else if ("searchCityByName".equals(action)) {
                JSONArray ja = searchCityByName(args.getString(0));
                callbackContext.success(ja);
            } else if ("getAllUpdateInfo".equals(action)) {
                JSONArray ja = getAllUpdateInfo();
                callbackContext.success(ja);
            } else if ("getUpdateInfo".equals(action)) {
                JSONArray ja = getUpdateInfoByID(args.getInt(0));
                callbackContext.success(ja);
            } else if ("downLoad".equals(action)) {
                downLoad(args.getInt(0));
            } else if ("update".equals(action)) {
                update(args.getInt(0));
            } else if ("pause".equals(action)) {
                pause(args.getInt(0));
            } else if ("remove".equals(action)) {
                remove(args.getInt(0));
            } else if ("getDistance".equals(action)) {
                JSONObject js = args.getJSONObject(0);
                JSONObject st = js.getJSONObject("start");
                JSONObject en = js.getJSONObject("end");
                LatLng start = new LatLng(st.getDouble("lat"), st.getDouble("lon"));
                LatLng end = new LatLng(en.getDouble("lat"), en.getDouble("lon"));
                String result = getDistance(start, end);
                callbackContext.success(result);
            } else if ("transCoords".equals(action)) {
                JSONObject js = coodsConvert(args.getJSONObject(0));
                callbackContext.success(js);
            }


        } catch (JSONException e) {
            e.printStackTrace();
            callbackContext.error(e.toString());
            return false;
        }
        return true;
    }

    void init(final JSONObject js, final CallbackContext callbackContext) {
        Boolean reOpenflag = false;
        try {
            JSONObject position = new JSONObject();
            if (js.has("position")) {
                position = js.getJSONObject("position");
            }
            activity = cordova.getActivity();
            DisplayMetrics dm = new DisplayMetrics();
            dm = activity.getApplicationContext().getResources().getDisplayMetrics();
            float density = dm.density;
            int width = 0;
            int height = 0;
            if (position.has("w") && position.has("h")) {
                width = Math.round(position.getInt("w") * density);
                height = Math.round(position.getInt("h") * density);

            } else {
                width = dm.widthPixels;
                height = dm.heightPixels;
            }
            // TODO 动态添加布局(java方式)
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams
                    (width, height);
            //设置底部
            if (position.has("x")) {
                params.rightMargin = Math.round(position.getInt("x") * density);
            } else {
                params.rightMargin = 0;
            }

            params.gravity = Gravity.TOP;
            if (position.has("y")) {
                params.topMargin = Math.round(position.getInt("y") * density);
            } else {
                params.topMargin = 0;
            }
            if (mMapView == null) {
                layout = new LinearLayout(activity);
                mMapView = new MapView(activity);
                layout.addView(mMapView);
                activity.addContentView(layout, params);
                mBaiduMap = mMapView.getMap();
//                mBaiduMap.setOnMapLoadedCallback(new BaiduMap.OnMapLoadedCallback() {
//                    @Override
//                    public void onMapLoaded() {
//                        loaded = true;
//                    }
//                });

                // 开启定位图层,为打开当前位置准备
                mBaiduMap.setMyLocationEnabled(true);
                // 定位初始化
                mLocClient = new LocationClient(activity);
                mLocClient.registerLocationListener(myListener);
                LocationClientOption option = new LocationClientOption();
                option.setOpenGps(true); // 打开gps
                option.setCoorType("bd09ll"); // 设置坐标类型
                option.setScanSpan(1000);
                mLocClient.setLocOption(option);
                mLocClient.start();
                mLocClient.requestLocation();

                //打开地图设置
                mUiSettings = mBaiduMap.getUiSettings();
                initListener(callbackContext);
            } else {
                cordova.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            JSONObject position = new JSONObject();
                            if (js.has("position")) {
                                position = js.getJSONObject("position");
                            }
                            activity = cordova.getActivity();
                            DisplayMetrics dm = new DisplayMetrics();
                            dm = activity.getApplicationContext().getResources().getDisplayMetrics();
                            float density = dm.density;
                            int width = 0;
                            int height = 0;
                            if (position.has("w") && position.has("h")) {
                                width = Math.round(position.getInt("w") * density);
                                height = Math.round(position.getInt("h") * density);
                            } else {
                                width = dm.widthPixels;
                                height = dm.heightPixels;
                            }
                            // TODO 动态添加布局(java方式)
                            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams
                                    (width, height);
                            //设置底部
                            if (position.has("x")) {
                                params.rightMargin = Math.round(position.getInt("x") * density);
                            } else {
                                params.rightMargin = 0;
                            }
                            params.gravity = Gravity.TOP;
                            if (position.has("y")) {
                                params.topMargin = Math.round(position.getInt("y") * density);
                            } else {
                                params.topMargin = 0;
                            }
                            layout.removeView(mMapView);
                            layout = new LinearLayout(activity);
                            layout.addView(mMapView);
                            activity.addContentView(layout, params);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });
                reOpenflag = true;
            }
            //设置地图参数
            if (js.has("center")) {
                JSONObject center = js.getJSONObject("center");
                setCenter(center.getDouble("lon"), center.getDouble("lat"));
            }
            double zoomLevel = 10;
            //默认值是10，取值范围超过3-21也取10
            if (js.has("zoomLevel")) {
                zoomLevel = js.getDouble("zoomLevel");
                if (zoomLevel < 3 || zoomLevel > 21) {
                    zoomLevel = 10;
                }
            }
            setZoomLevel(String.valueOf(zoomLevel));
//            Toast.makeText(activity,"zoomlevel:"+String.valueOf(zoomLevel),Toast.LENGTH_LONG).show();
            if (reOpenflag) {
                callbackContext.success();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param address 地址
     * @param city    城市名
     * @param cc      回调
     */
    //根据地址信息获取经纬度
    private void getLocationFromName(final String address, final String city, final CallbackContext cc) {
        activity = this.cordova.getActivity();
        new Thread() {

            @Override

            public void run() {
                String result = "";
                try {
                    //mcode是百度api的安全码
                    String mcode = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA).metaData.getString("mcode");
                    String baiduKey = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA).metaData.getString("com.baidu.lbsapi.API_KEY");
                    result = Http.Get(String.format("http://api.map.baidu.com/geocoder/v2/?address=%s&city=%s&output=json&ak=%s&mcode=%s", address, city, baiduKey, mcode));
                    JSONObject dataJson = new JSONObject(result);
                    dataJson = dataJson.getJSONObject("result");
                    dataJson = dataJson.getJSONObject("location");
                    result = dataJson.toString();
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                cc.success(result);
            }

        }.start();

    }

    /**
     * 根据经纬度获取地址信息
     *
     * @param latitud  纬度
     * @param longitud 经度
     * @param cc       回调
     */
    private void getNameFromLocation(final String latitud, final String longitud, final CallbackContext cc) {
        activity = this.cordova.getActivity();
        new Thread() {
            @Override

            public void run() {
            	JSONObject resultJson = null;
                String result = "";
                try {
                    //mcode是百度api的安全码
                    String mcode = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA).metaData.getString("mcode").trim();
                    String baiduKey = activity.getPackageManager().getApplicationInfo(activity.getPackageName(), PackageManager.GET_META_DATA).metaData.getString("com.baidu.lbsapi.API_KEY").trim();
                    result = Http.Get(String.format("http://api.map.baidu.com/geocoder/v2/?ak=%s&callback=renderReverse&location=%s,%s&output=json&pois=1&mcode=%s", baiduKey, latitud, longitud, mcode));
                    result = result.substring(result.indexOf("(")+1, result.lastIndexOf(")"));
                    try{
                    	resultJson =new JSONObject(result);                    
                    }catch (JSONException e) {
                    	throw new RuntimeException(e);
                    }
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                cc.success(resultJson);
            }
        }.start();

    }

    public CallbackContext getCallbackContext() {
        return callbackContext;
    }

    public void setCallbackContext(CallbackContext callbackContext) {
        this.callbackContext = callbackContext;
    }

    /**
     * android调用js的方法
     *
     * @param js
     */
    private void callJS(final String js) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl("javascript:" + js);
            }
        });
    }

    void offLineMapInit() {
        if (mOffline == null) {
            mOffline = new MKOfflineMap();
            mOffline.init(this);
        }
    }

    /**
     * 对地图事件的消息响应
     */
    private void initListener(final CallbackContext callbackContext) {
        mBaiduMap.setOnMapTouchListener(new BaiduMap.OnMapTouchListener() {

            @Override
            public void onTouch(MotionEvent event) {

            }
        });

        mBaiduMap.setOnMapLoadedCallback(new BaiduMap.OnMapLoadedCallback() {

            @Override
            public void onMapLoaded() {
                loaded = true;
                // TODO Auto-generated method stub
                callbackContext.success();
            }
        });
        mBaiduMap.setOnMapClickListener(new BaiduMap.OnMapClickListener() {
            /**
             * 单击地图
             */
            public void onMapClick(LatLng point) {
                try {
                    touchType = "单击地图";
                    currentPt = point;
                    updateMapState();
                    //		baiduMap.prototype.eventOccur
                    //		{action:String,lat:Num,lon:Num,zoom:Num,overlook:Num,rotate:Num}
                    JSONObject js = new JSONObject();
                    js.put("action", "click");
                    js.put("lat", point.latitude);
                    js.put("lon", point.longitude);
                    js.put("zoom", mBaiduMap.getMapStatus().zoom);
                    js.put("overlook", mBaiduMap.getMapStatus().overlook);
                    js.put("rotate", mBaiduMap.getMapStatus().rotate);

                    callJS("navigator.baiduMap.base.eventOccur(" + js + ")");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            /**
             * 单击地图中的POI点
             */
            public boolean onMapPoiClick(MapPoi poi) {
                touchType = "单击POI点";
                currentPt = poi.getPosition();
                updateMapState();
                listenResult = "onMapPoiClick";
                return false;
            }
        });

        mBaiduMap.setOnMapLongClickListener(new BaiduMap.OnMapLongClickListener() {
            /**
             * 长按地图
             */
            public void onMapLongClick(LatLng point) {
                touchType = "长按";
                currentPt = point;
                listenResult = "setOnMapLongClickListener";
                updateMapState();
                try {
                    JSONObject js = new JSONObject();
                    js.put("action", "longPress");
                    js.put("lat", point.latitude);
                    js.put("lon", point.longitude);
                    js.put("zoom", mBaiduMap.getMapStatus().zoom);
                    js.put("overlook", mBaiduMap.getMapStatus().overlook);
                    js.put("rotate", mBaiduMap.getMapStatus().rotate);

                    callJS("navigator.baiduMap.base.eventOccur(" + js + ")");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        mBaiduMap.setOnMapDoubleClickListener(new BaiduMap.OnMapDoubleClickListener() {
            /**
             * 双击地图
             */
            public void onMapDoubleClick(LatLng point) {
                touchType = "双击";
                currentPt = point;
                listenResult = "setOnMapDoubleClickListener";
                updateMapState();
                try {
                    JSONObject js = new JSONObject();
                    js.put("action", "dbClick");
                    js.put("lat", point.latitude);
                    js.put("lon", point.longitude);
                    js.put("zoom", mBaiduMap.getMapStatus().zoom);
                    js.put("overlook", mBaiduMap.getMapStatus().overlook);
                    js.put("rotate", mBaiduMap.getMapStatus().rotate);

                    callJS("navigator.baiduMap.base.eventOccur(" + js + ")");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        /**
         * 地图状态发生变化
         */
        mBaiduMap.setOnMapStatusChangeListener(new BaiduMap.OnMapStatusChangeListener() {
            public void onMapStatusChangeStart(MapStatus status) {
                updateMapState();
            }

            public void onMapStatusChangeFinish(MapStatus status) {
                updateMapState();
            }

            public void onMapStatusChange(MapStatus status) {
                updateMapState();
            }

        });

        mBaiduMap.setOnMarkerClickListener(new BaiduMap.OnMarkerClickListener() {
            public boolean onMarkerClick(final Marker marker) {

                InfoWindow.OnInfoWindowClickListener listener = null;

                listener = new InfoWindow.OnInfoWindowClickListener() {
                    public void onInfoWindowClick() {
                        LatLng ll = marker.getPosition();
                        LatLng llNew = new LatLng(ll.latitude + 0.005,
                                ll.longitude + 0.005);
                        marker.setPosition(llNew);
                        mBaiduMap.hideInfoWindow();
                    }
                };
                LatLng ll = marker.getPosition();


                return true;
            }
        });
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                updateMapState();
            }

        };
    }

    /**
     * 更新地图状态显示面板
     */
    private void updateMapState() {
        //调用js
        if (mStateBar == null) {
            return;
        }
        String state = "";
        if (currentPt == null) {
            state = "点击、长按、双击地图以获取经纬度和地图状态";
        } else {
            state = String.format(touchType + ",当前经度： %f 当前纬度：%f",
                    currentPt.longitude, currentPt.latitude);
        }
        state += "\n";
        MapStatus ms = mBaiduMap.getMapStatus();
        state += String.format(
                "zoom=%.1f rotate=%d overlook=%d",
                ms.zoom, (int) ms.rotate, (int) ms.overlook);
        mStateBar.setText(state);
    }

    /**
     * 定位SDK监听函数，接收服务返回的定位数据
     */
    public class MyLocationListenner implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            // map view 销毁后不在处理新接收的位置
            if (location == null || mMapView == null) {
                return;
            }
            //保存当前坐标
            currentLocation = new LatLng(location.getLatitude(),
                    location.getLongitude());
            currentBDLoc = location;
        }
    }

    /**
     * 获取当前位置经纬度
     *
     * @return String(JSONObject格式)
     */
    public static String getLocation() {
        //经纬度是服务形式获取的
        if (currentLocation != null) {
            JSONObject js = new JSONObject();
            try {
                long timestamp = System.currentTimeMillis();
                js.put("lat", currentLocation.latitude); //纬度
                js.put("lon", currentLocation.longitude);//经度
                js.put("timestamp", timestamp);   //时间戳
                return js.toString();
            } catch (JSONException ex) {
                ex.printStackTrace();
                return "";
            }
        }
        return "";
    }

    //是否在地图上显示当前位置，并且设置显示的样式
    public static void showCurrentLocation(Boolean isShow, String trackingMode) {
        MyLocationData locData = new MyLocationData.Builder()
                .accuracy(currentBDLoc.getRadius())
                // 此处设置开发者获取到的方向信息，顺时针0-360
                .direction(100).latitude(currentBDLoc.getLatitude())
                .longitude(currentBDLoc.getLongitude()).build();
        mBaiduMap.setMyLocationData(locData);
        if (isShow) {
            MapStatus.Builder builder = new MapStatus.Builder();
//            builder.target(currentLocation).zoom(18.0f);
            mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));
            mBaiduMap.setMyLocationEnabled(true);
        } else {
            mBaiduMap.setMyLocationEnabled(false);
        }

        // 修改为自定义marker
        mCurrentMarker = BitmapDescriptorFactory
                .fromResource(fakeR.getId("drawable", "icon_geo"));//R.drawable.icon_geo
        if (trackingMode.equals("compass")) {
            mCurrentMode = MyLocationConfiguration.LocationMode.COMPASS;
        } else if (trackingMode.equals("follow")) {
            mCurrentMode = MyLocationConfiguration.LocationMode.FOLLOWING;
        } else {  // if(trackingMode.toUpperCase().equals("NORMAL"))
            mCurrentMode = MyLocationConfiguration.LocationMode.NORMAL;
        }
        switch (mCurrentMode) {
            case NORMAL:
                mCurrentMode = MyLocationConfiguration.LocationMode.FOLLOWING;
                mBaiduMap
                        .setMyLocationConfigeration(new MyLocationConfiguration(
                                mCurrentMode, true, null));
                break;
            case COMPASS:
                mCurrentMode = MyLocationConfiguration.LocationMode.NORMAL;
                mBaiduMap
                        .setMyLocationConfigeration(new MyLocationConfiguration(
                                mCurrentMode, true, null));
                break;
            case FOLLOWING:
                mCurrentMode = MyLocationConfiguration.LocationMode.COMPASS;
                mBaiduMap
                        .setMyLocationConfigeration(new MyLocationConfiguration(
                                mCurrentMode, true, null));
                break;
            default:
                break;
        }

    }

    public static void setCenter(double longitude, double latitude) {
        LatLng centerpos = new LatLng(latitude, longitude); // 116.38507999999995   39.8713910804
        MapStatus.Builder builder = new MapStatus.Builder();
        builder.target(centerpos);
        mBaiduMap.setMapStatus(MapStatusUpdateFactory.newMapStatus(builder.build()));
    }

    public static String getCenter() {
        try {
            MapStatus status = mBaiduMap.getMapStatus();
            LatLng mCenterLatLng = status.target;
            /**获取经纬度*/
            double lat = mCenterLatLng.latitude;
            double lng = mCenterLatLng.longitude;
            JSONObject js = new JSONObject();
            js.put("lon", lng);
            js.put("lat", lat);
            return js.toString();
        } catch (JSONException e) {
            return "{error:" + e.toString() + "}";
        }
//        String result="经度:"+String.valueOf(lng)+",纬度:"+String.valueOf(lat);
//        Toast.makeText(activity,result,Toast.LENGTH_SHORT).show();
    }

    public static void setZoomLevel(String level) {
        try {
            float zoomLevel = Float.parseFloat(level);
            MapStatusUpdate u = MapStatusUpdateFactory.zoomTo(zoomLevel);
            mBaiduMap.setMapStatus(u);
//            Toast.makeText(this, "18.0", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private float getZoomLevel() {
        MapStatus status = mBaiduMap.getMapStatus();
        float zoomLevel = status.zoom;
        return zoomLevel;
    }

    public static void setMapAttr(String type, Boolean zoomEnable, Boolean scrollEnable) {
        if (type != null && type.equals("standard")) {
            //普通地图
            mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
        } else if (type != null && type.equals("satellite")) {
            //卫星地图
            mBaiduMap.setMapType(BaiduMap.MAP_TYPE_SATELLITE);
        } else if (type != null && type.equals("none")) {
            //空白
            mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NONE);
        }
        mMapView.showZoomControls(zoomEnable);
        setScrollEnable(scrollEnable);

    }

    public static void setRotation(float degrees) {
        MapStatus ms = new MapStatus.Builder(mBaiduMap.getMapStatus()).rotate(degrees).build();
        MapStatusUpdate u = MapStatusUpdateFactory.newMapStatus(ms);
        mBaiduMap.animateMapStatus(u);
    }

    public static void setOverlook(float degrees) {
        MapStatus ms = new MapStatus.Builder(mBaiduMap.getMapStatus()).overlook(degrees).build();
        MapStatusUpdate u = MapStatusUpdateFactory.newMapStatus(ms);
        mBaiduMap.animateMapStatus(u);
    }

    public static void setScaleBar(Boolean isShow, int x, int y) {
        mMapView.showScaleControl(isShow);
        mMapView.setScrollX(x);
        mMapView.setScrollY(y);
    }

    public static void setCompass(int x, int y) {

        setCompassEnable(true);
        Point pt = new Point();
        pt.set(x, y);
        mBaiduMap.setCompassPosition(pt);
    }

    public static void setTraffic(Boolean isShow) {
        mBaiduMap.setTrafficEnabled(isShow);
    }

    public static void setHeatMap(Boolean isShow) {
        mBaiduMap.setBaiduHeatMapEnabled(isShow);
    }

    public static void setBuilding(Boolean isShow) {
        mBaiduMap.setBuildingsEnabled(isShow);
    }

    private void setPosition(final JSONObject js) {
        cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                try {

                    JSONObject position = new JSONObject();
                    if (js.has("position")) {
                        position = js.getJSONObject("position");
                    }
                    activity = cordova.getActivity();
                    DisplayMetrics dm = new DisplayMetrics();
                    dm = activity.getApplicationContext().getResources().getDisplayMetrics();
                    float density = dm.density;
                    int width = 0;
                    int height = 0;
                    if (position.has("w") && position.has("h")) {
                        width = Math.round(position.getInt("w") * density);
                        height = Math.round(position.getInt("h") * density);

                    } else {
                        width = dm.widthPixels;
                        height = dm.heightPixels;
                    }
                    // TODO 动态添加布局(java方式)
                    FrameLayout.LayoutParams params = new FrameLayout.LayoutParams
                            (width, height);
                    //设置底部
                    if (position.has("x")) {
                        params.rightMargin = Math.round(position.getInt("x") * density);
                    } else {
                        params.rightMargin = 0;
                    }

                    params.gravity = Gravity.TOP;
                    if (position.has("y")) {
                        params.topMargin = Math.round(position.getInt("y") * density);
                    } else {
                        params.topMargin = 0;
                    }
                    layout.removeView(mMapView);
                    layout = new LinearLayout(activity);
                    layout.addView(mMapView);
                    activity.addContentView(layout, params);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
    }

    private void setRegion(JSONObject js) {
        try {
            LatLng northeast;
            LatLng southwest;
            if (js.has("lonDelta")) {
                northeast = new LatLng(js.getJSONObject("center").getDouble("lat") + js.getDouble("latDelta") / 2, js.getJSONObject("center").getDouble("lon") + js.getDouble("lonDelta") / 2);
                southwest = new LatLng(js.getJSONObject("center").getDouble("lat") - js.getDouble("latDelta") / 2, js.getJSONObject("center").getDouble("lon") - js.getDouble("lonDelta") / 2);
            } else {
                northeast = new LatLng(js.getJSONObject("northeast").getDouble("lat"), js.getJSONObject("northeast").getDouble("lon"));
                southwest = new LatLng(js.getJSONObject("southwest").getDouble("lat"), js.getJSONObject("southwest").getDouble("lon"));
            }
            LatLngBounds bounds = new LatLngBounds.Builder().include(northeast)
                    .include(southwest).build();
            MapStatusUpdate u = MapStatusUpdateFactory.newLatLngBounds(bounds);
            if (js.has("animation") && js.getBoolean("animation") && loaded) {
                mBaiduMap.animateMapStatus(u);
            } else if (loaded) {
                mBaiduMap.setMapStatus(u);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private JSONObject getRegion() {
//        "lonDelta":Number - 矩形区域经度差,
//                "latDelta":Number - 矩形区域纬度差,
//                "center":Object 中心点的经纬度,例如:{lon:116.397, lat:39.910},
        JSONObject js = new JSONObject();
        try {
//            MapStatus ms = mBaiduMap.getMapStatus();
//            LatLng northeast = ms.bound.northeast;
//            LatLng southwest = ms.bound.southwest;
//            JSONObject north = new JSONObject();
//            JSONObject sourth = new JSONObject();
//
//            north.put("lat", northeast.latitude);
//            north.put("lon", northeast.longitude);
//            sourth.put("lat", southwest.latitude);
//            sourth.put("lon", southwest.longitude);

//            js.put("northeast", north);
//            js.put("southwest", sourth);
            JSONObject center = new JSONObject(getCenter());
            double lonDelta = (northeast.longitude - center.getDouble("lon")) * 2;
            double latDelta = (northeast.latitude - center.getDouble("lat")) * 2;
            js.put("lonDelta", lonDelta);
            js.put("latDelta", latDelta);
            js.put("center", center);
//            Toast.makeText(activity,
//                    "lonDelta：" + lonDelta+"  latDelta:"+latDelta,
//                    Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return js;
    }

    public static void zoomIn() {
        MapStatusUpdate u = MapStatusUpdateFactory.zoomIn();
        mBaiduMap.animateMapStatus(u);
    }

    public static void zoomOut() {
        MapStatusUpdate u = MapStatusUpdateFactory.zoomOut();
        mBaiduMap.animateMapStatus(u);
    }

    public static void addAnnotations(JSONArray ja) {
        try {
            JSONObject js = new JSONObject();
//            Iterator it = js.keys();
//            while (it.hasNext()) {
            for (int i = 0; i < ja.length(); i++) {
                js = ja.getJSONObject(i);
//            {id:1,title:"标题",subTitle:"子标题",lon:20.1,lat:12.3,draggable:true,color:navigator.baiduMap.annotationColor.Red,bgImgPath:"背景图片的路径，要求必须是本地路径"}
                LatLng latLng = new LatLng(js.getDouble("lat"), js.getDouble("lon"));
                //需要增加自定义图片的支持
                BitmapDescriptor bd = BitmapDescriptorFactory
                        .fromResource(fakeR.getId("drawable", "icon_gcoding"));//R.drawable.icon_gcoding
                MarkerOptions oo = new MarkerOptions().position(latLng)
                        .icon(bd).zIndex(9).draggable(true)
                        .title(js.getString("title"));
                Marker marker = (Marker) (mBaiduMap.addOverlay(oo));

                Info info = new Info();
                info.setMarker(marker);
                info.setId(js.getString("id"));
                listOop.add(info);
                //设置覆盖物是否可被拖拽
                Boolean draggable = js.getBoolean("draggable");
                marker.setDraggable(draggable);
                mBaiduMap.setOnMarkerDragListener(new BaiduMap.OnMarkerDragListener() {
                    public void onMarkerDrag(Marker marker) {
                    }

                    public void onMarkerDragEnd(Marker marker) {
//                    Toast.makeText(
//                            activity,
//                            "拖拽结束，新位置：" + marker.getPosition().latitude + ", "
//                                    + marker.getPosition().longitude,
//                            Toast.LENGTH_LONG).show();
                    }

                    public void onMarkerDragStart(Marker marker) {
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 覆写此方法设置要管理的Overlay列表
     *
     * @return 管理的Overlay列表
     */
//    public abstract List<OverlayOptions> getOverlayOptions();
    public static void removeAnnotations(JSONArray jsonArray) {
//
//        mBaiduMap.clear();
        String id = "";
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                id = jsonArray.getString(i);
                for (int n = 0; n < listOop.size(); n++) {
                    if (listOop.get(n).getId().equals(id)) {
                        listOop.get(n).getMarker().remove();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void removeAllAnno() {
        try {
            if (listOop != null && listOop.size() > 0) {
                for (int n = 0; n < listOop.size(); n++) {
                    listOop.get(n).getMarker().remove();
                }
                listOop.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static LatLng getAnnotationCoords(String id) {
        LatLng la = new LatLng(0, 0);
        try {
            for (int n = 0; n < listOop.size(); n++) {
                if (listOop.get(n).getId().equals(id)) {
                    la = listOop.get(n).getMarker().getPosition();
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return la;
    }

    public static void updateAnnotationCoords(JSONObject ja) {
        try {
            LatLng pt = new LatLng(ja.getDouble("lat"), ja.getDouble("lon"));
            String id = ja.getString("id");
            for (int n = 0; n < listOop.size(); n++) {
                if (listOop.get(n).getId().equals(id)) {
                    listOop.get(n).getMarker().setPosition(pt);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static boolean annotationExist(String id) {
        Boolean result = false;
        try {
            for (int n = 0; n < listOop.size(); n++) {
                if (listOop.get(n).getId().equals(id)) {
                    result = true;
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void addLine(JSONObject js) {
        try {
            JSONArray ja = js.getJSONArray("points");
            Double lonTmp = 0.0;
            Double latTmp = 0.0;
            // 普通折线，点击时改变宽度
            Polyline mPolyline;


            List<LatLng> points = new ArrayList<LatLng>();
            for (int i = 0; i < ja.length(); i++) {
                JSONObject jsonObject = ja.getJSONObject(i);
                lonTmp = jsonObject.getDouble("lon");
                latTmp = jsonObject.getDouble("lat");
//                Point pointS = new Point(latTmp,lonTmp);
                LatLng latLng = new LatLng(latTmp, lonTmp);
                points.add(latLng);
            }
//            Color.parseColor(styles.getString("borderColor"))
            JSONObject styles = js.getJSONObject("styles");
            OverlayOptions ooPolyline = new PolylineOptions().width(10)
                    .color(Color.parseColor(styles.getString("borderColor"))).points(points).width(styles.getInt("borderWidth"));
            overlay = mBaiduMap.addOverlay(ooPolyline);
            Info info = new Info();
            info.setId(js.getString("id"));
            info.setMyOverlay(overlay);
            listOop.add(info);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addPolygon(JSONObject js) {
        try {
            JSONArray ja = js.getJSONArray("points");
            Double lonTmp = 0.0;
            Double latTmp = 0.0;
            // 普通折线，点击时改变宽度
            Polyline mPolyline;


            List<LatLng> points = new ArrayList<LatLng>();
            for (int i = 0; i < ja.length(); i++) {
                JSONObject jsonObject = ja.getJSONObject(i);
                lonTmp = jsonObject.getDouble("lon");
                latTmp = jsonObject.getDouble("lat");
//                Point pointS = new Point(latTmp,lonTmp);
                LatLng latLng = new LatLng(latTmp, lonTmp);
                points.add(latLng);
            }
//            Color.parseColor(styles.getString("borderColor"))
            JSONObject styles = js.getJSONObject("styles");
//            OverlayOptions ooPolyline = new PolylineOptions().width(10)
//                    .color(Color.parseColor(styles.getString("borderColor"))).points(points).width(styles.getInt("borderWidth"));

//            borderColor:"#000000",fillColor:"#6495ED",borderWidth:3,alpha:"0.2"}
            OverlayOptions ooPolygon = new PolygonOptions().points(points)
                    .stroke(new Stroke(styles.getInt("borderWidth"), Color.parseColor(styles.getString("borderColor")))).fillColor(Color.parseColor(styles.getString("fillColor")));
            overlay = mBaiduMap.addOverlay(ooPolygon);

//            mPolyline = (Polyline) mBaiduMap.addOverlay(ooPolyline);
            Info info = new Info();
            info.setId(js.getString("id"));
            info.setMyOverlay(overlay);
            listOop.add(info);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addArc(JSONObject js) {
        // 添加弧线
        try {
            JSONArray ja = js.getJSONArray("points");
            Double lonTmp = 0.0;
            Double latTmp = 0.0;
            // 普通折线，点击时改变宽度
            Polyline mPolyline;


            List<LatLng> points = new ArrayList<LatLng>();
            for (int i = 0; i < ja.length(); i++) {
                JSONObject jsonObject = ja.getJSONObject(i);
                lonTmp = jsonObject.getDouble("lon");
                latTmp = jsonObject.getDouble("lat");

                LatLng latLng = new LatLng(latTmp, lonTmp);
                points.add(latLng);
            }

            JSONObject styles = js.getJSONObject("styles");

//            OverlayOptions ooPolygon = new PolygonOptions().points(points)
//                    .stroke(new Stroke(styles.getInt("borderWidth"), Color.parseColor(styles.getString("borderColor")))).fillColor(Color.parseColor(styles.getString("fillColor")));
//            mPolyline=(Polyline) mBaiduMap.addOverlay(ooPolygon);
            OverlayOptions ooArc = new ArcOptions().color(Color.parseColor(styles.getString("borderColor"))).width(styles.getInt("borderWidth"))
                    .points(points.get(0), points.get(1), points.get(2));
            overlay = mBaiduMap.addOverlay(ooArc);

            Info info = new Info();
            info.setId(js.getString("id"));
            info.setMyOverlay(overlay);
            listOop.add(info);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public static void addCircle(JSONObject js) {
        // 添加圆
        try {
            JSONObject ja = js.getJSONObject("center");
            Double lonTmp = 0.0;
            Double latTmp = 0.0;


            lonTmp = ja.getDouble("lon");
            latTmp = ja.getDouble("lat");

            LatLng latLng = new LatLng(latTmp, lonTmp);


            JSONObject styles = js.getJSONObject("styles");

//            OverlayOptions ooPolygon = new PolygonOptions().points(points)
//                    .stroke(new Stroke(styles.getInt("borderWidth"), Color.parseColor(styles.getString("borderColor")))).fillColor(Color.parseColor(styles.getString("fillColor")));
//            mPolyline=(Polyline) mBaiduMap.addOverlay(ooPolygon);

            OverlayOptions ooCircle = new CircleOptions().fillColor(Color.parseColor(styles.getString("fillColor")))
                    .center(latLng).stroke(new Stroke(styles.getInt("borderWidth"), Color.parseColor(styles.getString("borderColor"))))
                    .radius(js.getInt("radius"));
            overlay = mBaiduMap.addOverlay(ooCircle);
            Info info = new Info();
            info.setId(js.getString("id"));
            info.setMyOverlay(overlay);
            listOop.add(info);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void removeOverlay(JSONArray jsonArray) {
        String id = "";
        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                id = jsonArray.getString(i);
                for (int n = 0; n < listOop.size(); n++) {
                    if (listOop.get(n).getId().equals(id)) {
                        listOop.get(n).getMyOverlay().remove();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        mPolyline.remove();

    }

    /**
     * 下载离线地图
     *
     * @param cityId
     */
    public static void downLoad(int cityId) {
        mOffline.start(cityId);
    }

    /**
     * 更新离线地图
     *
     * @param cityId
     */
    public static void update(int cityId) {
        mOffline.update(cityId);
    }

    /**
     * 停止下载地图
     *
     * @param cityId
     */
    public static void pause(int cityId) {
        mOffline.pause(cityId);
    }

    /**
     * 移除离线地图
     *
     * @param cityId
     */
    public static void remove(int cityId) {
        mOffline.remove(cityId);
    }

    public static String getDistance(LatLng start, LatLng end) {

        double distance = DistanceUtil.getDistance(start, end);
        return String.valueOf(distance);
    }


    @Override
    public void onMapClick(LatLng point) {
        mBaiduMap.hideInfoWindow();
    }

    @Override
    public boolean onMapPoiClick(MapPoi poi) {
        return false;
    }


    //地图设置

    /**
     * 是否启用缩放手势
     *
     * @param swith
     */
    public static void setZoomEnable(Boolean swith) {
        mUiSettings.setZoomGesturesEnabled(swith);
    }

    /**
     * 是否启用平移手势
     *
     * @param swith
     */
    public static void setScrollEnable(Boolean swith) {
        mUiSettings.setScrollGesturesEnabled(swith);
    }

    /**
     * 是否启用旋转手势
     *
     * @param swith
     */
    public static void setRotateEnable(Boolean swith) {
        mUiSettings.setRotateGesturesEnabled(swith);
    }

    /**
     * 是否启用俯视手势
     *
     * @param swith
     */
    public static void setOverlookEnable(Boolean swith) {
        mUiSettings.setOverlookingGesturesEnabled(swith);
    }

    /**
     * 交通地图模式
     *
     * @param swith
     */
    public void setTrafficMap(Boolean swith) {
        mBaiduMap.setTrafficEnabled(true);
    }

    /**
     * 是否启用指南针图层
     *
     * @param swith
     */
    public static void setCompassEnable(Boolean swith) {
        mUiSettings.setCompassEnabled(swith);
    }

    //热门城市
    public static JSONArray getHotCityList() {

        JSONArray cityAll = new JSONArray();
        try {
            //size cityName cityID cityType childCities

//            ArrayList<String> hotCities = new ArrayList<String>();
            ArrayList<MKOLSearchRecord> records1 = mOffline.getHotCityList();
            if (records1 != null) {
                for (MKOLSearchRecord r : records1) {
                    JSONObject cityPrams = new JSONObject();
                    cityPrams.put("size", r.size);
                    cityPrams.put("cityName", r.cityName);
                    cityPrams.put("cityID", r.cityID);
                    cityPrams.put("cityType", r.cityType);
                    if (r.childCities == null) {
                        cityPrams.put("childCities", "");
                    } else {
                        cityPrams.put("childCities", r.childCities);
                    }
//                    hotCities.add(r.cityName + "(" + r.cityID + ")" + "   --"
//                            + this.formatDataSize(r.size));
                    cityAll.put(cityPrams);
                }
            }
            return cityAll;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    /**
     * 获取离线地图城市列表
     *
     * @return
     */
    public static JSONArray getOfflineCityList() {
        JSONArray cityAll = new JSONArray();
        try {
            //size cityName cityID cityType childCities

//            ArrayList<String> hotCities = new ArrayList<String>();
            ArrayList<MKOLSearchRecord> records1 = mOffline.getOfflineCityList();
            if (records1 != null) {
                for (MKOLSearchRecord r : records1) {
                    JSONObject cityPrams = new JSONObject();
                    cityPrams.put("size", r.size);
                    cityPrams.put("cityName", r.cityName);
                    cityPrams.put("cityID", r.cityID);
                    cityPrams.put("cityType", r.cityType);
                    if (r.childCities == null) {
                        cityPrams.put("childCities", "");
                    } else {
                        cityPrams.put("childCities", r.childCities);
                    }
                    cityAll.put(cityPrams);
                }
            }
            return cityAll;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 根据城市名搜索离线地图记录
     *
     * @param cityName
     * @return
     */
    public static JSONArray searchCityByName(String cityName) {
        JSONArray cityAll = new JSONArray();
        try {
            //size cityName cityID cityType childCities

//            ArrayList<String> hotCities = new ArrayList<String>();
            ArrayList<MKOLSearchRecord> records1 = mOffline.searchCity(cityName);
            if (records1 != null) {
                for (MKOLSearchRecord r : records1) {
                    JSONObject cityPrams = new JSONObject();
                    cityPrams.put("size", r.size);
                    cityPrams.put("cityName", r.cityName);
                    cityPrams.put("cityID", r.cityID);
                    cityPrams.put("cityType", r.cityType);
                    if (r.childCities == null) {
                        cityPrams.put("childCities", "");
                    } else {
                        cityPrams.put("childCities", r.childCities);
                    }
                    cityAll.put(cityPrams);
                }
            }
            return cityAll;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JSONArray getAllUpdateInfo() {
        JSONArray cityAll = new JSONArray();
        try {
            ArrayList<MKOLUpdateElement> records = mOffline.getAllUpdateInfo();
            if (records != null) {
                for (MKOLUpdateElement r : records) {
                    JSONObject cityPrams = new JSONObject();
                    cityPrams.put("size", r.size);
                    cityPrams.put("cityName", r.cityName);
                    cityPrams.put("cityID", r.cityID);
                    cityPrams.put("ratio", r.ratio);
                    cityPrams.put("status", r.status);
                    cityPrams.put("serversize", r.serversize);
                    cityPrams.put("level", r.level);
                    cityPrams.put("update", r.update);
                    cityPrams.put("lat", r.geoPt.latitude);
                    cityPrams.put("lon", r.geoPt.latitude);

                    cityAll.put(cityPrams);
                }
            }
            return cityAll;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JSONArray getUpdateInfoByID(int cityID) {
        JSONArray cityAll = new JSONArray();
        try {
            MKOLUpdateElement records = mOffline.getUpdateInfo(cityID);
            if (records != null) {
                JSONObject cityPrams = new JSONObject();
                cityPrams.put("size", records.size);
                cityPrams.put("cityName", records.cityName);
                cityPrams.put("cityID", records.cityID);
                cityPrams.put("ratio", records.ratio);
                cityPrams.put("status", records.status);
                cityPrams.put("serversize", records.serversize);
                cityPrams.put("level", records.level);
                cityPrams.put("update", records.update);
                cityPrams.put("lat", records.geoPt.latitude);
                cityPrams.put("lon", records.geoPt.latitude);

                cityAll.put(cityPrams);
            }
            return cityAll;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String formatDataSize(int size) {
        String ret = "";
        if (size < (1024 * 1024)) {
            ret = String.format("%dK", size / 1024);
        } else {
            ret = String.format("%.1fM", size / (1024 * 1024.0));
        }
        return ret;
    }

    @Override
    public void onGetOfflineMapState(int type, int state) {
        switch (type) {
            case MKOfflineMap.TYPE_DOWNLOAD_UPDATE: {
                MKOLUpdateElement update = mOffline.getUpdateInfo(state);
                // 处理下载进度更新提示
                if (update != null) {
//                    stateView.setText(String.format("%s : %d%%", update.cityName,
//                            update.ratio));
//                    updateView();
                    try {
                        JSONObject js = new JSONObject();
                        js.put("cityID", update.cityID);
                        js.put("cityName", update.cityName);
                        js.put("ratio", update.ratio);
                        ccForOffine.success(js);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//					Toast.makeText(cordova.getActivity(),update.ratio,Toast.LENGTH_SHORT).show();
                }
            }
            break;
            case MKOfflineMap.TYPE_NEW_OFFLINE:
                // 有新离线地图安装
                Log.d("OfflineDemo", String.format("add offlinemap num:%d", state));
                ccForOffine.success(state);
                break;
            case MKOfflineMap.TYPE_VER_UPDATE:
                // 版本更新提示
                MKOLUpdateElement e = mOffline.getUpdateInfo(state);

                break;
            default:
                break;
        }

    }

    public static JSONObject coodsConvert(JSONObject js) {
        LatLng desLatLng = new LatLng(0.0, 0.0);
        JSONObject result = new JSONObject();
        try {
            if (js.getString("type").equals("gps")) {
                LatLng ll = new LatLng(js.getDouble("lat"), js.getDouble("lon"));
                // 将google地图、soso地图、aliyun地图、mapabc地图和amap地图// 所用坐标转换成百度坐标
                CoordinateConverter converter = new CoordinateConverter();
                converter.from(CoordinateConverter.CoordType.COMMON);
                // sourceLatLng待转换坐标
                converter.coord(ll);
                desLatLng = converter.convert();
            } else if (js.getString("type").equals("common")) {
                LatLng ll = new LatLng(js.getDouble("lat"), js.getDouble("lon"));
                // 将GPS设备采集的原始GPS坐标转换成百度坐标
                CoordinateConverter converter = new CoordinateConverter();
                converter.from(CoordinateConverter.CoordType.GPS);
                // sourceLatLng待转换坐标
                converter.coord(ll);
                desLatLng = converter.convert();
            }
            result.put("lon", desLatLng.longitude);
            result.put("lat", desLatLng.latitude);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

}