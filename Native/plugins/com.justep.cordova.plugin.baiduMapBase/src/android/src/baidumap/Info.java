package baidumap;

import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.Overlay;
import java.io.Serializable;

/**
 * Created by spark on 16/8/2.
 */
public class Info implements Serializable {
    public Info() {
    }

    private String id;
    private Marker marker;
    private Overlay myOverlay;



    public Info(String id, Marker marker) {
        super();
        this.id = id;
        this.marker = marker;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public Overlay getMyOverlay() {
        return myOverlay;
    }

    public void setMyOverlay(Overlay myOverlay) {
        this.myOverlay = myOverlay;
    }

}