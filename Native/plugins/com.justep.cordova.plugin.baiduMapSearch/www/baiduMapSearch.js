var cordova = require('cordova');
var exec = require('cordova/exec');

var baiduMapSearch = function(){	
};

//搜索线路的类型
baiduMapSearch.prototype.routeType = {
	drive : "drive",
	transit : "transit",
	walk : "walk"
};
//搜索线路时的策略
baiduMapSearch.prototype.routePolicy = {
	ecar_fee_first : 0,//驾乘检索策略常量：较少费用
	ecar_dis_first : 1,//驾乘检索策略常量：最短距离
	ecar_time_first : 2,//驾乘检索策略常量：时间优先
	ecar_avoid_jam : 3,//驾乘检索策略常量：躲避拥堵
	ebus_no_subway : 4,//公交检索策略常量：不含地铁
	ebus_time_first : 5,//公交检索策略常量：时间优先
	ebus_transfer_first : 6,//公交检索策略常量：最少换乘
	ebus_walk_first : 7//公交检索策略常量：最少步行距离
};
// //开始搜索GPS信息（卫星个数，以及每个卫星的信噪比数组），本接口仅支持 android 平台
// baiduMapSearch.prototype.startSearchGPS = function(suceess,error){
// 	exec(suceess,error,"baiduMapSearch","startSearchGPS",[]);
// };
// //停止搜索GPS信息，本接口仅支持 android 平台
// baiduMapSearch.prototype.stopSearchGPS = function(suceess,error){
// 	exec(suceess,error,"baiduMapSearch","stopSearchGPS",[]);
// };
//搜索路线方案，无需调用 open 接口即可使用
baiduMapSearch.prototype.searchRoute = function(args,suceess,error){
	exec(suceess,error,"baiduMapSearch","searchRoute",[args]);
};
//在地图上显示指定路线，调用本接口前，必须保证已经调用过接口 open 和 searchRoute
baiduMapSearch.prototype.drawRoute = function(args){
	exec(null,null,"baiduMapSearch","drawRoute",[args]);
};
//根据id移除指定线路图
baiduMapSearch.prototype.removeRoute = function(args){
	exec(null,null,"baiduMapSearch","removeRoute",[args]);
};
//根据关键字搜索公交、地铁线路，无需调用 open 接口即可搜索
baiduMapSearch.prototype.searchBusRoute = function(args,suceess,error){
	exec(suceess,error,"baiduMapSearch","searchBusRoute",[args]);
};
//根据 searchBusRoute 搜索返回的查询线路详情并绘制在地图上
baiduMapSearch.prototype.drawBusRoute = function(args){
	exec(null,null,"baiduMapSearch","drawBusRoute",[args]);
};
//根据单个关键字搜索兴趣点，无需调用 open 接口即可搜索
baiduMapSearch.prototype.searchInCity = function(args,suceess,error){
	exec(suceess,error,"baiduMapSearch","searchInCity",[args]);
};
//根据单个关键字在圆形区域内搜索兴趣点，无需调用 open 接口即可搜索
baiduMapSearch.prototype.searchNearby = function(args,suceess,error){
	exec(suceess,error,"baiduMapSearch","searchNearby",[args]);
};

module.exports = new baiduMapSearch();