package baidumap.search;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaRouter;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.CircleOptions;
import com.baidu.mapapi.map.GroundOverlayOptions;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.Overlay;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.Stroke;
import com.baidu.mapapi.map.UiSettings;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.LatLngBounds;
import com.baidu.mapapi.overlayutil.BikingRouteOverlay;
import com.baidu.mapapi.overlayutil.BusLineOverlay;
import com.baidu.mapapi.overlayutil.DrivingRouteOverlay;
import com.baidu.mapapi.overlayutil.PoiOverlay;
import com.baidu.mapapi.overlayutil.TransitRouteOverlay;
import com.baidu.mapapi.overlayutil.WalkingRouteOverlay;
import com.baidu.mapapi.search.busline.BusLineResult;
import com.baidu.mapapi.search.busline.BusLineSearch;
import com.baidu.mapapi.search.busline.BusLineSearchOption;
import com.baidu.mapapi.search.busline.OnGetBusLineSearchResultListener;
import com.baidu.mapapi.search.core.CityInfo;
import com.baidu.mapapi.search.core.PoiInfo;
import com.baidu.mapapi.search.core.RouteLine;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiCitySearchOption;
import com.baidu.mapapi.search.poi.PoiDetailResult;
import com.baidu.mapapi.search.poi.PoiDetailSearchOption;
import com.baidu.mapapi.search.poi.PoiIndoorResult;
import com.baidu.mapapi.search.poi.PoiNearbySearchOption;
import com.baidu.mapapi.search.poi.PoiResult;
import com.baidu.mapapi.search.poi.PoiSearch;
import com.baidu.mapapi.search.poi.PoiSortType;
import com.baidu.mapapi.search.route.BikingRoutePlanOption;
import com.baidu.mapapi.search.route.BikingRouteResult;
import com.baidu.mapapi.search.route.DrivingRoutePlanOption;
import com.baidu.mapapi.search.route.DrivingRouteResult;
import com.baidu.mapapi.search.route.OnGetRoutePlanResultListener;
import com.baidu.mapapi.search.route.PlanNode;
import com.baidu.mapapi.search.route.RoutePlanSearch;
import com.baidu.mapapi.search.route.TransitRoutePlanOption;
import com.baidu.mapapi.search.route.TransitRouteResult;
import com.baidu.mapapi.search.route.WalkingRoutePlanOption;
import com.baidu.mapapi.search.route.WalkingRouteResult;
import com.baidu.mapapi.search.sug.OnGetSuggestionResultListener;
import com.baidu.mapapi.search.sug.SuggestionResult;
import com.baidu.mapapi.utils.CoordinateConverter;
import com.baidu.mapapi.utils.DistanceUtil;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import baidumap.BaiduMapPlugin;
import baidumap.FakeR;
import baidumap.Info;
import util.Http;

//import com.justep.newbaidu.FragmentHtml;


@SuppressLint("UseSparseArrays")
public class BaiduSearchPlugin extends CordovaPlugin implements BaiduMap.OnMapClickListener,
        OnGetRoutePlanResultListener,OnGetPoiSearchResultListener, OnGetBusLineSearchResultListener,OnGetSuggestionResultListener {

    public CallbackContext callbackContext;
    public static Activity activity;

    private static FakeR fakeR;
    /**
     * MapView 是地图主控件
     */
    public static MapView mMapView;
    public static BaiduMap mBaiduMap;


    /**
     * 当前地点击点
     */
    private LatLng currentPt;
    /**
     * 当前定位地址
     */
    public static LatLng currentLocation;

    /**
     * 当前定位地址
     */
    static BDLocation currentBDLoc;


    private String touchType;
    private String listenResult;

    // 定位相关
    LocationClient mLocClient;
    public MyLocationListenner myListener = new MyLocationListenner();
    private static MyLocationConfiguration.LocationMode mCurrentMode;


    public static List<RouteInfo> listRouteInfos =new ArrayList<RouteInfo>();
//    static Overlay overlay = null ;

    // 浏览路线节点相关
//    Button mBtnPre = null; // 上一个节点
//    Button mBtnNext = null; // 下一个节点
    int nodeIndex = -1; // 节点索引,供浏览节点时使用
    RouteLine route = null;
    com.baidu.mapapi.overlayutil.OverlayManager routeOverlay = null;
    boolean useDefaultIcon = false;
    //画线
    TransitRouteResult nowResult = null;
    DrivingRouteResult nowResultd  = null;

    private TextView popupText = null; // 泡泡view


    // 搜索相关
    static RoutePlanSearch mSearch = null;    // 搜索模块，也可去掉地图模块独立使用

    //检索用
    static PoiSearch mPoiSearch = null;
    private List<String> suggest;
    /**
     * 搜索关键字输入窗口
     */

    private AutoCompleteTextView keyWorldsView = null;
    private ArrayAdapter<String> sugAdapter = null;
    static int loadIndex = 0;

    static LatLng center = new LatLng(39.92235, 116.380338);
    static int radius = 500;
    LatLng southwest = new LatLng( 39.92235, 116.380338 );
    LatLng northeast = new LatLng( 39.947246, 116.414977);
    LatLngBounds searchbound = new LatLngBounds.Builder().include(southwest).include(northeast).build();

    static int searchType = 0;  // 搜索的类型，在显示时区分
    /**
     * 用于显示地图状态的面板
     */
    private TextView mStateBar;
    //地图设置
    public  static UiSettings mUiSettings;

//    //显示窗口
//    public static WindowManager wm;
//    private static View mView = null;

    //公交线路检索
//    private int nodeIndex = -2; // 节点索引,供浏览节点时使用
    private BusLineResult broute = null; // 保存驾车/步行路线数据的变量，供浏览节点时使用
    static List<String> busLineIDList = null;
    static int busLineIndex = 0;
    // 搜索相关
    static PoiSearch bSearch = null; // 搜索模块，也可去掉地图模块独立使用
    static BusLineSearch mBusLineSearch = null;
    BusLineOverlay boverlay; // 公交路线绘制对象
    //Marker信息显示
//    private InfoWindow mInfoWindow;

    @Override
    public boolean execute(String action, final JSONArray args,
                           final CallbackContext callbackContext) {
        setCallbackContext(callbackContext);

        if(mMapView==null) {
            fakeR = new FakeR(cordova.getActivity());
			SDKInitializer.initialize(cordova.getActivity().getApplicationContext());
            mMapView= BaiduMapPlugin.mMapView;
            init();
        }

        try {
            if("searchRoute".equals(action)) {
                JSONObject js= args.getJSONObject(0);
                searchRoute(js);
            }else if("drawRoute".equals(action)) {
            }else if("searchBusRoute".equals(action)) {
                JSONObject js= args.getJSONObject(0);
                searchBusRoute(js.getString("city"),js.getString("line"));
            }else if("removeRoute".equals(action)) {
                removeRoute();
            }else if("searchInCity".equals(action)) {
                JSONObject js= args.getJSONObject(0);
                searchInCity(js.getString("city"),js.getString("key"));
            }else if("searchNearby".equals(action)) {
                JSONObject js=args.getJSONObject(0);
                searchNearby(js);
            }
//            else if("drawBusRoute".equals(action)) {
//            }

        }catch (JSONException e){
            e.printStackTrace();
            callbackContext.error(e.toString());
            return  false;
        }
        return true;
    }
    void init(){

        try {
            activity = cordova.getActivity();
            mBaiduMap = mMapView.getMap();

            // 开启定位图层,为打开当前位置准备
            mBaiduMap.setMyLocationEnabled(true);
            // 定位初始化
            mLocClient = new LocationClient(activity);
            mLocClient.registerLocationListener(myListener);
            LocationClientOption option = new LocationClientOption();
            option.setOpenGps(true); // 打开gps
            option.setCoorType("bd09ll"); // 设置坐标类型
            option.setScanSpan(1000);
            mLocClient.setLocOption(option);
            mLocClient.start();
            mLocClient.requestLocation();

//		mOffline = new MKOfflineMap();
//		mOffline.init(this);
            // 初始化搜索模块，注册事件监听
            mSearch = RoutePlanSearch.newInstance();
            mSearch.setOnGetRoutePlanResultListener(this);


//		mBtnPre = (Button) activity.findViewById(fakeR.getId("id", "pre"));
//		mBtnNext = (Button) activity.findViewById(fakeR.getId("id", "next"));
//		mBtnPre.setVisibility(View.INVISIBLE);
//		mBtnNext.setVisibility(View.INVISIBLE);


            // 初始化城市搜索模块，注册搜索事件监听
            mPoiSearch = PoiSearch.newInstance();
            mPoiSearch.setOnGetPoiSearchResultListener(this);
            bSearch = PoiSearch.newInstance();
            bSearch.setOnGetPoiSearchResultListener(this);


            mBusLineSearch = BusLineSearch.newInstance();
            mBusLineSearch.setOnGetBusLineSearchResultListener(this);
            busLineIDList = new ArrayList<String>();
            boverlay = new BusLineOverlay(mBaiduMap);
            mBaiduMap.setOnMarkerClickListener(boverlay);

            //打开地图设置
            mUiSettings = mBaiduMap.getUiSettings();

            initListener();
        }catch (Exception e){
            e.printStackTrace();
        }
    }



    public CallbackContext getCallbackContext() {
        return callbackContext;
    }

    public void setCallbackContext(CallbackContext callbackContext) {
        this.callbackContext = callbackContext;
    }

    /** android调用js的方法
     * @param js
     */
    private void callJS(final String js) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                webView.loadUrl("javascript:" + js);
            }
        });
    }
    /**
     * 对地图事件的消息响应
     */
    private void initListener() {
        mBaiduMap.setOnMapTouchListener(new BaiduMap.OnMapTouchListener() {

            @Override
            public void onTouch(MotionEvent event) {

            }
        });


        mBaiduMap.setOnMapClickListener(new BaiduMap.OnMapClickListener() {
            /**
             * 单击地图
             */
            public void onMapClick(LatLng point) {
                try {
                    touchType = "单击地图";
                    currentPt = point;
                    updateMapState();
                    //		baiduMap.prototype.eventOccur
                    //		{action:String,lat:Num,lon:Num,zoom:Num,overlook:Num,rotate:Num}
                    JSONObject js = new JSONObject();
                    js.put("action", "click");
                    js.put("lat", point.latitude);
                    js.put("lon", point.longitude);
                    js.put("zoom", mBaiduMap.getMapStatus().zoom);
                    js.put("overlook", mBaiduMap.getMapStatus().overlook);
                    js.put("rotate", mBaiduMap.getMapStatus().rotate);

                    callJS("navigator.baiduMap.base.eventOccur("+js+")");
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }

            /**
             * 单击地图中的POI点
             */
            public boolean onMapPoiClick(MapPoi poi) {
                touchType = "单击POI点";
                currentPt = poi.getPosition();
                updateMapState();
                listenResult="onMapPoiClick";
                return false;
            }
        });

        mBaiduMap.setOnMapLongClickListener(new BaiduMap.OnMapLongClickListener() {
            /**
             * 长按地图
             */
            public void onMapLongClick(LatLng point) {
                touchType = "长按";
                currentPt = point;
                listenResult="setOnMapLongClickListener";
                updateMapState();
                try{
                    JSONObject js = new JSONObject();
                    js.put("action", "longPress");
                    js.put("lat", point.latitude);
                    js.put("lon", point.longitude);
                    js.put("zoom", mBaiduMap.getMapStatus().zoom);
                    js.put("overlook", mBaiduMap.getMapStatus().overlook);
                    js.put("rotate", mBaiduMap.getMapStatus().rotate);

                    callJS("navigator.baiduMap.base.eventOccur("+js+")");
                }catch (JSONException e){
                    e.printStackTrace();
                }

            }
        });
        mBaiduMap.setOnMapDoubleClickListener(new BaiduMap.OnMapDoubleClickListener() {
            /**
             * 双击地图
             */
            public void onMapDoubleClick(LatLng point) {
                touchType = "双击";
                currentPt = point;
                listenResult="setOnMapDoubleClickListener";
                updateMapState();
                try{
                    JSONObject js = new JSONObject();
                    js.put("action", "dbClick");
                    js.put("lat", point.latitude);
                    js.put("lon", point.longitude);
                    js.put("zoom", mBaiduMap.getMapStatus().zoom);
                    js.put("overlook", mBaiduMap.getMapStatus().overlook);
                    js.put("rotate", mBaiduMap.getMapStatus().rotate);

                    callJS("navigator.baiduMap.base.eventOccur("+js+")");
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        });

        /**
         * 地图状态发生变化
         */
        mBaiduMap.setOnMapStatusChangeListener(new BaiduMap.OnMapStatusChangeListener() {
            public void onMapStatusChangeStart(MapStatus status) {
                updateMapState();
            }

            public void onMapStatusChangeFinish(MapStatus status) {
                updateMapState();
            }

            public void onMapStatusChange(MapStatus status) {
                updateMapState();
                try{
                    JSONObject center = new JSONObject(getCenter());
                    JSONObject js = new JSONObject();
                    js.put("action", "viewChange");
                    js.put("lat", center.getString("lat"));
                    js.put("lon", center.getString("lon"));
                    js.put("zoom", mBaiduMap.getMapStatus().zoom);
                    js.put("overlook", mBaiduMap.getMapStatus().overlook);
                    js.put("rotate", mBaiduMap.getMapStatus().rotate);

                    callJS("navigator.baiduMap.base.eventOccur("+js+")");
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        });
//		setZoomLevelbtn= (Button) activity.findViewById(R.id.setZoomLevelbtn);
//		getZoomLevelbtn= (Button) activity.findViewById(R.id.getZoomLevelbtn);
        mBaiduMap.setOnMarkerClickListener(new BaiduMap.OnMarkerClickListener() {
            public boolean onMarkerClick(final Marker marker) {

                InfoWindow.OnInfoWindowClickListener listener = null;

                listener = new InfoWindow.OnInfoWindowClickListener() {
                    public void onInfoWindowClick() {
                        LatLng ll = marker.getPosition();
                        LatLng llNew = new LatLng(ll.latitude + 0.005,
                                ll.longitude + 0.005);
                        marker.setPosition(llNew);
                        mBaiduMap.hideInfoWindow();
                    }
                };
                return true;
            }
        });
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                updateMapState();
            }

        };
//		setZoomLevelbtn.setOnClickListener(onClickListener);
//		getZoomLevelbtn.setOnClickListener(onClickListener);
    }

    /**
     * 更新地图状态显示面板
     */
    private void updateMapState() {
        //调用js
        if (mStateBar == null) {
            return;
        }
        String state = "";
        if (currentPt == null) {
            state = "点击、长按、双击地图以获取经纬度和地图状态";
        } else {
            state = String.format(touchType + ",当前经度： %f 当前纬度：%f",
                    currentPt.longitude, currentPt.latitude);
        }
        state += "\n";
        MapStatus ms = mBaiduMap.getMapStatus();
        state += String.format(
                "zoom=%.1f rotate=%d overlook=%d",
                ms.zoom, (int) ms.rotate, (int) ms.overlook);
        mStateBar.setText(state);
    }
    /**
     * 定位SDK监听函数，接收服务返回的定位数据
     */
    public class MyLocationListenner implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            // map view 销毁后不在处理新接收的位置
            if (location == null || mMapView == null) {
                return;
            }
            //保存当前坐标
            currentLocation = new LatLng(location.getLatitude(),
                    location.getLongitude());
            currentBDLoc=location;
        }

        public void onReceivePoi(BDLocation poiLocation) {
        }
    }

    /**
     * 获取当前位置经纬度
     * @return String(JSONObject格式)
     */
    public static String getLocation(){
        //经纬度是服务形式获取的
        if(currentLocation!=null){
            JSONObject js = new JSONObject();
            try {
                long timestamp=System.currentTimeMillis();
                js.put("lat", currentLocation.latitude); //纬度
                js.put("lon", currentLocation.longitude);//经度
                js.put("timestamp", timestamp);   //时间戳
                return js.toString();
            }catch (JSONException ex){
                ex.printStackTrace();
                return "";
            }
        }
        return "";
    }

    public static String getCenter(){
        try {
            MapStatus status = mBaiduMap.getMapStatus();
            LatLng mCenterLatLng = status.target;
            /**获取经纬度*/
            double lat = mCenterLatLng.latitude;
            double lng = mCenterLatLng.longitude;
            JSONObject js = new JSONObject();
            js.put("lon", lng);
            js.put("lat", lat);
            return js.toString();
        }catch (JSONException e){
            return "{error:"+e.toString()+"}";
        }
    }
    public static void searchRoute(JSONObject js){
        try {
            // 设置起终点信息，对于tranist search 来说，城市名无意义
            PlanNode stNode = PlanNode.withCityNameAndPlaceName(js.getString("sCityName"), js.getString("startName"));
            PlanNode enNode = PlanNode.withCityNameAndPlaceName(js.getString("eCityName"), js.getString("endName"));
            String type=js.getString("type");
            // 实际使用中请对起点终点城市进行正确的设定
            if (js.getString("type").equals("drive")) {
                mSearch.drivingSearch((new DrivingRoutePlanOption())
                        .from(stNode).to(enNode));
            } else if (type.equals("transit")) {
                mSearch.transitSearch((new TransitRoutePlanOption())
                        .from(stNode).city(js.getString("sCityName")).to(enNode));
            } else if (type.equals("walk")) {
                mSearch.walkingSearch((new WalkingRoutePlanOption())
                        .from(stNode).to(enNode));
            } else if (type.equals("bike")) {
                mSearch.bikingSearch((new BikingRoutePlanOption())
                        .from(stNode).to(enNode));
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public static void searchBusRoute(String cityName,String keyWord){
        try {
            searchType = 5;
            bSearch.searchInCity((new PoiCitySearchOption()).city(
                    cityName)
                    .keyword(keyWord));
            mBusLineSearch.searchBusLine((new BusLineSearchOption()
                    .city(cityName).uid(busLineIDList.get(busLineIndex))));
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public static void removeRoute(){
        try {
            if (listRouteInfos.size() > 0) {
                for (int i = 0; i < listRouteInfos.size(); i++) {
                    listRouteInfos.get(i).getRouteOverlay().removeFromMap();
                }
                listRouteInfos.clear();
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    public static void searchInCity(String city,String key){
            mPoiSearch.searchInCity((new PoiCitySearchOption())
                    .city(city).keyword(key).pageNum(loadIndex));

    }
    public static void searchNearby(JSONObject js){

        try {
            LatLng latLng=new LatLng(js.getDouble("lat"),js.getDouble("lon"));
            searchType = 2;
            PoiNearbySearchOption nearbySearchOption = new PoiNearbySearchOption().keyword(js.getString("keyword")).sortType(PoiSortType.distance_from_near_to_far).location(latLng)
                    .radius(js.getInt("radius")).pageNum(loadIndex);
            mPoiSearch.searchNearby(nearbySearchOption);
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    /** 获取行走路线
     * @param result
     */
    @Override
    public void onGetWalkingRouteResult(WalkingRouteResult result) {
        if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
            Toast.makeText(activity, "抱歉，未找到结果", Toast.LENGTH_SHORT).show();
        }
        if (result.error == SearchResult.ERRORNO.AMBIGUOUS_ROURE_ADDR) {
            // 起终点或途经点地址有岐义，通过以下接口获取建议查询信息
//             result.getSuggestAddrInfo();
            return;
        }
        if (result.error == SearchResult.ERRORNO.NO_ERROR) {
            nodeIndex = -1;
//            mBtnPre.setVisibility(View.VISIBLE);
//            mBtnNext.setVisibility(View.VISIBLE);
            route = result.getRouteLines().get(0);
            WalkingRouteOverlay overlay = new MyWalkingRouteOverlay(mBaiduMap);
            mBaiduMap.setOnMarkerClickListener(overlay);
            routeOverlay = overlay;
            overlay.setData(result.getRouteLines().get(0));
            overlay.addToMap();
            overlay.zoomToSpan();

            RouteInfo  routeInfo =new RouteInfo();
            routeInfo.setRouteOverlay(overlay);
            listRouteInfos.add(routeInfo);
            JSONObject jsonObject =new JSONObject();
            try {
                jsonObject.put("distance", route.getDistance());
                jsonObject.put("duration", route.getDuration());
                jsonObject.put("distance", route.getStarting());
                jsonObject.put("distance", route.getTerminal());
                jsonObject.put("distance", route.getTitle());
                jsonObject.put("distance", route.getAllStep());


            }catch (JSONException je){
                je.printStackTrace();
            }
        }
    }

    /**
     *  获取公交地铁路线
     * @param result  路线查询结果
     */
    @Override
    public void onGetTransitRouteResult(TransitRouteResult result) {

        if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
            Toast.makeText(activity, "抱歉，未找到结果", Toast.LENGTH_SHORT).show();
        }
        if (result.error == SearchResult.ERRORNO.AMBIGUOUS_ROURE_ADDR) {
            // 起终点或途经点地址有岐义，通过以下接口获取建议查询信息
            // result.getSuggestAddrInfo()
            return;
        }
        if (result.error == SearchResult.ERRORNO.NO_ERROR) {
            nodeIndex = -1;
//            mBtnPre.setVisibility(View.VISIBLE);
//            mBtnNext.setVisibility(View.VISIBLE);


            if (result.getRouteLines().size() > 1 ) {
                nowResult = result;

                MyTransitDlg myTransitDlg = new MyTransitDlg(activity,
                        result.getRouteLines(),
                        RouteLineAdapter.Type.TRANSIT_ROUTE);
                myTransitDlg.setOnItemInDlgClickLinster(new OnItemInDlgClickListener() {
                    public void onItemClick(int position) {
                        route = nowResult.getRouteLines().get(position);
                        TransitRouteOverlay overlay = new MyTransitRouteOverlay(mBaiduMap);
                        mBaiduMap.setOnMarkerClickListener(overlay);
                        routeOverlay = overlay;
                        overlay.setData(nowResult.getRouteLines().get(position));
                        overlay.addToMap();
                        overlay.zoomToSpan();

                        RouteInfo  routeInfo =new RouteInfo();
                        routeInfo.setRouteOverlay(overlay);
                        listRouteInfos.add(routeInfo);
                        JSONObject jsonObject =new JSONObject();
//                        route.getAllStep()
                    }

                });
                myTransitDlg.show();



            } else if ( result.getRouteLines().size() == 1 ) {
                // 直接显示
                route = result.getRouteLines().get(0);
                TransitRouteOverlay overlay = new MyTransitRouteOverlay(mBaiduMap);
                mBaiduMap.setOnMarkerClickListener(overlay);
                routeOverlay = overlay;
                overlay.setData(result.getRouteLines().get(0));
                overlay.addToMap();
                overlay.zoomToSpan();
                RouteInfo  routeInfo =new RouteInfo();
                routeInfo.setRouteOverlay(overlay);
                listRouteInfos.add(routeInfo);
                JSONObject jsonObject =new JSONObject();
            } else {
                Log.d("transitresult", "结果数<0" );
                return;
            }


        }
    }

    /** 获取驾车路线
     * @param result
     */
    @Override
    public void onGetDrivingRouteResult(DrivingRouteResult result) {
        if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
            Toast.makeText(activity, "抱歉，未找到结果", Toast.LENGTH_SHORT).show();
        }
        if (result.error == SearchResult.ERRORNO.AMBIGUOUS_ROURE_ADDR) {
            // 起终点或途经点地址有岐义，通过以下接口获取建议查询信息
            // result.getSuggestAddrInfo()
            return;
        }
        if (result.error == SearchResult.ERRORNO.NO_ERROR) {
            nodeIndex = -1;
//            mBtnPre.setVisibility(View.VISIBLE);
//            mBtnNext.setVisibility(View.VISIBLE);

            if (result.getRouteLines().size() > 1 ) {
                nowResultd = result;

                MyTransitDlg myTransitDlg = new MyTransitDlg(activity,
                        result.getRouteLines(),
                        RouteLineAdapter.Type.DRIVING_ROUTE);
                myTransitDlg.setOnItemInDlgClickLinster(new OnItemInDlgClickListener() {
                    public void onItemClick(int position) {
                        route = nowResultd.getRouteLines().get(position);
                        DrivingRouteOverlay overlay = new MyDrivingRouteOverlay(mBaiduMap);
                        mBaiduMap.setOnMarkerClickListener(overlay);
                        routeOverlay = overlay;
                        overlay.setData(nowResultd.getRouteLines().get(position));
                        overlay.addToMap();
                        overlay.zoomToSpan();
                        RouteInfo  routeInfo =new RouteInfo();
                        routeInfo.setRouteOverlay(overlay);
                        listRouteInfos.add(routeInfo);
                        JSONObject jsonObject =new JSONObject();
                    }

                });
                myTransitDlg.show();

            } else if ( result.getRouteLines().size() == 1 ) {
                route = result.getRouteLines().get(0);
                DrivingRouteOverlay overlay = new MyDrivingRouteOverlay(mBaiduMap);
                routeOverlay = overlay;
                mBaiduMap.setOnMarkerClickListener(overlay);
                overlay.setData(result.getRouteLines().get(0));
                overlay.addToMap();
                overlay.zoomToSpan();
//                mBtnPre.setVisibility(View.VISIBLE);
//                mBtnNext.setVisibility(View.VISIBLE);
            }

        }
    }

    /**  获取骑行路线
     * @param bikingRouteResult
     */
    @Override
    public void onGetBikingRouteResult(BikingRouteResult bikingRouteResult) {
        if (bikingRouteResult == null || bikingRouteResult.error != SearchResult.ERRORNO.NO_ERROR) {
            Toast.makeText(activity, "抱歉，未找到结果", Toast.LENGTH_SHORT).show();
        }
        if (bikingRouteResult.error == SearchResult.ERRORNO.AMBIGUOUS_ROURE_ADDR) {
            // 起终点或途经点地址有岐义，通过以下接口获取建议查询信息
            // result.getSuggestAddrInfo()
            return;
        }
        if (bikingRouteResult.error == SearchResult.ERRORNO.NO_ERROR) {
            nodeIndex = -1;
//            mBtnPre.setVisibility(View.VISIBLE);
//            mBtnNext.setVisibility(View.VISIBLE);
            route = bikingRouteResult.getRouteLines().get(0);
            BikingRouteOverlay overlay = new MyBikingRouteOverlay(mBaiduMap);
            routeOverlay = overlay;
            mBaiduMap.setOnMarkerClickListener(overlay);
            overlay.setData(bikingRouteResult.getRouteLines().get(0));
            overlay.addToMap();
            overlay.zoomToSpan();
            RouteInfo  routeInfo =new RouteInfo();
            routeInfo.setRouteOverlay(overlay);
            listRouteInfos.add(routeInfo);
            JSONObject jsonObject =new JSONObject();
        }
    }

    /** 定制RouteOverly
     *
     */
    private class MyDrivingRouteOverlay extends DrivingRouteOverlay {

        public MyDrivingRouteOverlay(BaiduMap baiduMap) {
            super(baiduMap);
        }

        @Override
        public BitmapDescriptor getStartMarker() {
            if (useDefaultIcon) {
                return BitmapDescriptorFactory.fromResource(fakeR.getId("drawable", "icon_st"));//R.drawable.icon_st
            }
            return null;
        }

        @Override
        public BitmapDescriptor getTerminalMarker() {
            if (useDefaultIcon) {
                return BitmapDescriptorFactory.fromResource(fakeR.getId("drawable", "icon_en"));//R.drawable.icon_en
            }
            return null;
        }
    }
    //    /**
//     * 节点浏览示例
//     *
//     * @param v
//     */
//    public void nodeClick(View v) {
//        if (route == null || route.getAllStep() == null) {
//            return;
//        }
//        if (nodeIndex == -1 && v.getId() == R.id.pre) {
//            return;
//        }
//        // 设置节点索引
//        if (v.getId() == R.id.next) {
//            if (nodeIndex < route.getAllStep().size() - 1) {
//                nodeIndex++;
//            } else {
//                return;
//            }
//        } else if (v.getId() == R.id.pre) {
//            if (nodeIndex > 0) {
//                nodeIndex--;
//            } else {
//                return;
//            }
//        }
//        // 获取节结果信息
//        LatLng nodeLocation = null;
//        String nodeTitle = null;
//        Object step = route.getAllStep().get(nodeIndex);
//        if (step instanceof DrivingRouteLine.DrivingStep) {
//            nodeLocation = ((DrivingRouteLine.DrivingStep) step).getEntrance().getLocation();
//            nodeTitle = ((DrivingRouteLine.DrivingStep) step).getInstructions();
//        } else if (step instanceof WalkingRouteLine.WalkingStep) {
//            nodeLocation = ((WalkingRouteLine.WalkingStep) step).getEntrance().getLocation();
//            nodeTitle = ((WalkingRouteLine.WalkingStep) step).getInstructions();
//        } else if (step instanceof TransitRouteLine.TransitStep) {
//            nodeLocation = ((TransitRouteLine.TransitStep) step).getEntrance().getLocation();
//            nodeTitle = ((TransitRouteLine.TransitStep) step).getInstructions();
//        } else if (step instanceof BikingRouteLine.BikingStep) {
//            nodeLocation = ((BikingRouteLine.BikingStep) step).getEntrance().getLocation();
//            nodeTitle = ((BikingRouteLine.BikingStep) step).getInstructions();
//        }
//
//        if (nodeLocation == null || nodeTitle == null) {
//            return;
//        }
//        // 移动节点至中心
//        mBaiduMap.setMapStatus(MapStatusUpdateFactory.newLatLng(nodeLocation));
//        // show popup
//        popupText = new TextView(activity);
//        popupText.setBackgroundResource(R.drawable.popup);
//        popupText.setTextColor(0xFF000000);
//        popupText.setText(nodeTitle);
//        mBaiduMap.showInfoWindow(new InfoWindow(popupText, nodeLocation, 0));
//
//    }
    private class MyWalkingRouteOverlay extends WalkingRouteOverlay {

        public MyWalkingRouteOverlay(BaiduMap baiduMap) {
            super(baiduMap);
        }

        @Override
        public BitmapDescriptor getStartMarker() {
            if (useDefaultIcon) {
                return BitmapDescriptorFactory.fromResource(fakeR.getId("drawable", "icon_st"));//R.drawable.icon_st
            }
            return null;
        }

        @Override
        public BitmapDescriptor getTerminalMarker() {
            if (useDefaultIcon) {
                return BitmapDescriptorFactory.fromResource(fakeR.getId("drawable", "icon_en"));//R.drawable.icon_en
            }
            return null;
        }
    }

    private class MyTransitRouteOverlay extends TransitRouteOverlay {

        public MyTransitRouteOverlay(BaiduMap baiduMap) {
            super(baiduMap);
        }

        @Override
        public BitmapDescriptor getStartMarker() {
            if (useDefaultIcon) {
                return BitmapDescriptorFactory.fromResource(fakeR.getId("drawable", "icon_st"));//R.drawable.icon_st
            }
            return null;
        }

        @Override
        public BitmapDescriptor getTerminalMarker() {
            if (useDefaultIcon) {
                return BitmapDescriptorFactory.fromResource(fakeR.getId("drawable", "icon_en"));//R.drawable.icon_en
            }
            return null;
        }
    }
    private class MyBikingRouteOverlay extends BikingRouteOverlay {
        public  MyBikingRouteOverlay(BaiduMap baiduMap) {
            super(baiduMap);
        }

        @Override
        public BitmapDescriptor getStartMarker() {
            if (useDefaultIcon) {
                return BitmapDescriptorFactory.fromResource(fakeR.getId("drawable", "icon_st"));//R.drawable.icon_st
            }
            return null;
        }

        @Override
        public BitmapDescriptor getTerminalMarker() {
            if (useDefaultIcon) {
                return BitmapDescriptorFactory.fromResource(fakeR.getId("drawable", "icon_en"));//R.drawable.icon_en
            }
            return null;
        }
    }
    @Override
    public void onMapClick(LatLng point) {
        mBaiduMap.hideInfoWindow();
    }

    @Override
    public boolean onMapPoiClick(MapPoi poi) {
        return false;
    }

    // 供路线选择的Dialog
    class MyTransitDlg extends Dialog {

        private List<? extends RouteLine> mtransitRouteLines;
        private ListView transitRouteList;
        private RouteLineAdapter mTransitAdapter;

        OnItemInDlgClickListener onItemInDlgClickListener;

        public MyTransitDlg(Context context, int theme) {
            super(context, theme);
        }

        public MyTransitDlg(Context context, List< ? extends RouteLine> transitRouteLines, RouteLineAdapter.Type
                type) {
            this( context, 0);
            mtransitRouteLines = transitRouteLines;
            mTransitAdapter = new RouteLineAdapter( context, mtransitRouteLines , type);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(fakeR.getId("layout", "activity_transit_dialog"));//R.layout.activity_transit_dialog

            transitRouteList = (ListView) findViewById(fakeR.getId("id", "transitList"));
            transitRouteList.setAdapter(mTransitAdapter);

            transitRouteList.setOnItemClickListener( new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    onItemInDlgClickListener.onItemClick( position);
//                    mBtnPre.setVisibility(View.VISIBLE);
//                    mBtnNext.setVisibility(View.VISIBLE);
                    dismiss();

                }
            });
        }

        public void setOnItemInDlgClickLinster( OnItemInDlgClickListener itemListener) {
            onItemInDlgClickListener = itemListener;
        }

    }
    // 响应DLg中的List item 点击
    interface OnItemInDlgClickListener {
        public void onItemClick(int position);
    }
    /**
     * 获取POI搜索结果，包括searchInCity，searchNearby，searchInBound返回的搜索结果
     * @param result
     */
    public void onGetPoiResult(PoiResult result) {
        if (result == null || result.error == SearchResult.ERRORNO.RESULT_NOT_FOUND) {
            Toast.makeText(activity, "未找到结果", Toast.LENGTH_LONG)
                    .show();
            return;
        }
        if (result.error == SearchResult.ERRORNO.NO_ERROR) {
            mBaiduMap.clear();
            PoiOverlay overlay = new MyPoiOverlay(mBaiduMap);
            mBaiduMap.setOnMarkerClickListener(overlay);
            overlay.setData(result);
            overlay.addToMap();
            overlay.zoomToSpan();
            switch( searchType ) {
                case 2:
                    showNearbyArea(center, radius);
                    break;
                case 3:
                    showBound(searchbound);
                    break;
                case 5:
                    // 遍历所有poi，找到类型为公交线路的poi
                    busLineIDList.clear();
                    for (PoiInfo poi : result.getAllPoi()) {
                        if (poi.type == PoiInfo.POITYPE.BUS_LINE
                                || poi.type == PoiInfo.POITYPE.SUBWAY_LINE) {
                            busLineIDList.add(poi.uid);
                        }
                    }
                    searchNextBusline(null);
                    break;
                default:
                    break;
            }

            return;
        }
        if (result.error == SearchResult.ERRORNO.AMBIGUOUS_KEYWORD) {

            // 当输入关键字在本市没有找到，但在其他城市找到时，返回包含该关键字信息的城市列表
            String strInfo = "在";
            for (CityInfo cityInfo : result.getSuggestCityList()) {
                strInfo += cityInfo.city;
                strInfo += ",";
            }
            strInfo += "找到结果";
            Toast.makeText(activity, strInfo, Toast.LENGTH_LONG)
                    .show();
        }

    }

    /**
     * 获取POI详情搜索结果，得到searchPoiDetail返回的搜索结果
     * @param result
     */
    public void onGetPoiDetailResult(PoiDetailResult result) {
        if (result.error != SearchResult.ERRORNO.NO_ERROR) {
            Toast.makeText(activity, "抱歉，未找到结果", Toast.LENGTH_SHORT)
                    .show();
        } else {
            Toast.makeText(activity, result.getName() + ": " + result.getAddress(), Toast.LENGTH_SHORT)
                    .show();
        }
    }

    @Override
    public void onGetPoiIndoorResult(PoiIndoorResult poiIndoorResult) {

    }

    /**
     * 获取在线建议搜索结果，得到requestSuggestion返回的搜索结果
     * @param res
     */
    @Override
    public void onGetSuggestionResult(SuggestionResult res) {
        if (res == null || res.getAllSuggestions() == null) {
            return;
        }
        suggest = new ArrayList<String>();
        for (SuggestionResult.SuggestionInfo info : res.getAllSuggestions()) {
            if (info.key != null) {
                suggest.add(info.key);
            }
        }
        sugAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_dropdown_item_1line, suggest);
        keyWorldsView.setAdapter(sugAdapter);
        sugAdapter.notifyDataSetChanged();
    }
    private class MyPoiOverlay extends PoiOverlay {

        public MyPoiOverlay(BaiduMap baiduMap) {
            super(baiduMap);
        }

        @Override
        public boolean onPoiClick(int index) {
            super.onPoiClick(index);
            PoiInfo poi = getPoiResult().getAllPoi().get(index);
            // if (poi.hasCaterDetails) {
            mPoiSearch.searchPoiDetail((new PoiDetailSearchOption())
                    .poiUid(poi.uid));
            // }
            return true;
        }
    }

    /**
     * 对周边检索的范围进行绘制
     * @param center
     * @param radius
     */
    public void showNearbyArea( LatLng center, int radius) {
        BitmapDescriptor centerBitmap = BitmapDescriptorFactory
                .fromResource(fakeR.getId("drawable", "icon_geo"));//R.drawable.icon_geo
        MarkerOptions ooMarker = new MarkerOptions().position(center).icon(centerBitmap);
        mBaiduMap.addOverlay(ooMarker);

        OverlayOptions ooCircle = new CircleOptions().fillColor( 0xCCCCCC00 )
                .center(center).stroke(new Stroke(5, 0xFFFF00FF ))
                .radius(radius);
        mBaiduMap.addOverlay(ooCircle);
    }

    /**
     * 对区域检索的范围进行绘制
     * @param bounds
     */
    public void showBound( LatLngBounds bounds) {
        BitmapDescriptor bdGround = BitmapDescriptorFactory
                .fromResource(fakeR.getId("drawable", "ground_overlay"));//R.drawable.ground_overlay

        OverlayOptions ooGround = new GroundOverlayOptions()
                .positionFromBounds(bounds).image(bdGround).transparency(0.8f);
        mBaiduMap.addOverlay(ooGround);

        MapStatusUpdate u = MapStatusUpdateFactory
                .newLatLng(bounds.getCenter());
        mBaiduMap.setMapStatus(u);

        bdGround.recycle();
    }


    public void searchNextBusline(View v) {
        if (busLineIndex >= busLineIDList.size()) {
            busLineIndex = 0;
        }
        if (busLineIndex >= 0 && busLineIndex < busLineIDList.size()
                && busLineIDList.size() > 0) {
            mBusLineSearch.searchBusLine((new BusLineSearchOption()
                    .city("北京").uid(busLineIDList.get(busLineIndex))));

            busLineIndex++;
        }

    }

    /**
     * 节点浏览示例
     *
     * @param v
     */
    public void nodeClick(View v) {

        if (nodeIndex < -1 || broute == null
                || nodeIndex >= broute.getStations().size()) {
            return;
        }
        TextView popupText = new TextView(activity
        );
        popupText.setBackgroundResource(fakeR.getId("drawable", "popup"));//R.drawable.popup
        popupText.setTextColor(0xff000000);
//        // 上一个节点
//        if (mBtnPre.equals(v) && nodeIndex > 0) {
//            // 索引减
//            nodeIndex--;
//        }
//        // 下一个节点
//        if (mBtnNext.equals(v) && nodeIndex < (broute.getStations().size() - 1)) {
//            // 索引加
//            nodeIndex++;
//        }
        if (nodeIndex >= 0) {
            // 移动到指定索引的坐标
            mBaiduMap.setMapStatus(MapStatusUpdateFactory.newLatLng(broute
                    .getStations().get(nodeIndex).getLocation()));
            // 弹出泡泡
            popupText.setText(broute.getStations().get(nodeIndex).getTitle());
            mBaiduMap.showInfoWindow(new InfoWindow(popupText, broute.getStations()
                    .get(nodeIndex).getLocation(), 10));
        }
    }
    @Override
    public void onGetBusLineResult(BusLineResult result) {
        if (result == null || result.error != SearchResult.ERRORNO.NO_ERROR) {
            Toast.makeText(activity, "抱歉，未找到结果",
                    Toast.LENGTH_LONG).show();
            return;
        }
        mBaiduMap.clear();
        broute = result;
        nodeIndex = -1;
        boverlay.removeFromMap();
        boverlay.setData(result);
        boverlay.addToMap();
        boverlay.zoomToSpan();
        RouteInfo  routeInfo =new RouteInfo();
        routeInfo.setRouteOverlay(boverlay);
        listRouteInfos.add(routeInfo);
//        mBtnPre.setVisibility(View.VISIBLE);
//        mBtnNext.setVisibility(View.VISIBLE);
        Toast.makeText(activity, result.getBusLineName(),
                Toast.LENGTH_SHORT).show();
    }

}