//
//  baiduMapSearch.mm
//  baiduMap
//
//  Created by LiangQiangkun on 16/5/20.
//
//

#import "baiduMapSearch.h"
#define MYBUNDLE_NAME @ "mapapi.bundle"
#define MYBUNDLE_PATH [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent: MYBUNDLE_NAME]
#define MYBUNDLE [NSBundle bundleWithPath: MYBUNDLE_PATH]
@interface RouteAnnotation : BMKPointAnnotation
{
    int _type; ///<0:起点 1：终点 2：公交 3：地铁 4:驾乘 5:途经点
    int _degree;
}
@property (nonatomic) int type;
@property (nonatomic) int degree;
@end
@implementation RouteAnnotation

@synthesize type = _type;
@synthesize degree = _degree;
@end
@implementation baiduMapSearch
{
    BMKRouteSearch *_rSearcher;
    BMKBusLineSearch *_bSearcher;
    BMKPoiSearch *_poiSearch;
    BMKMapView *_mapView;
    NSString *_city;
    NSMutableArray* _busPoiArray;
    int _currentIndex;
    NSString *_callBackId;
    NSNumber *_idNum;
    NSMutableArray *_overlayArray;
}
//插件初始化方法
-(void)pluginInitialize{
    _mapView = (BMKMapView *)[[baiduMapViewController sharedVC] view];
    _mapView.delegate = self;
    //创建检索对象
    _rSearcher = [[BMKRouteSearch alloc]init];
    _rSearcher.delegate = self;
    _bSearcher = [[BMKBusLineSearch alloc]init];
    _bSearcher.delegate = self;
    _poiSearch = [[BMKPoiSearch alloc]init];
    _poiSearch.delegate = self;
    _overlayArray = [[NSMutableArray alloc]init];
}
//搜索线路
- (void)searchRoute:(CDVInvokedUrlCommand *)command{
    _callBackId = command.callbackId;
    NSDictionary *params = [command argumentAtIndex:0];
    //准备参数
    NSString *type = [params objectForKey:@"type"];
    NSString *mode = [params objectForKey:@"mode"];
    NSNumber *policy = [params objectForKey:@"policy"];
    BMKPlanNode *startNode = [[BMKPlanNode alloc]init];
    BMKPlanNode *endNode = [[BMKPlanNode alloc]init];
    if ([mode isEqualToString:@"name"]) {
        //根据名字来确定起点终点
        NSString *sCityName = [params objectForKey:@"sCityName"];
        NSString *eCityName = [params objectForKey:@"eCityName"];
        NSString *startName = [params objectForKey:@"startName"];
        NSString *endName = [params objectForKey:@"endName"];
        startNode.cityName = sCityName;
        startNode.name = startName;
        endNode.name = endName;
        endNode.cityName = eCityName;
        
    }else if ([mode isEqualToString:@"coor"]){
        //根据坐标来确定起点终点
        NSDictionary *startDic = [params objectForKey:@"startCoor"];
        NSDictionary *endDic = [params objectForKey:@"endCoor"];
        CLLocationCoordinate2D start;
        start.latitude = [[startDic objectForKey:@"lat"] floatValue];
        start.longitude = [[startDic objectForKey:@"lon"] floatValue];
        startNode.pt = start;
        CLLocationCoordinate2D end;
        end.latitude = [[endDic objectForKey:@"lat"] floatValue];
        end.longitude = [[endDic objectForKey:@"lon"] floatValue];
        endNode.pt = end;
    }
    //判断检索的类型
    if ([type isEqualToString:@"walk"]) {
        //步行
        BMKWalkingRoutePlanOption *walkingRoutePlanOption = [[BMKWalkingRoutePlanOption alloc]init];
        walkingRoutePlanOption.from = startNode;
        walkingRoutePlanOption.to = endNode;
        BOOL flag = [_rSearcher walkingSearch:walkingRoutePlanOption];
        if (!flag) {
            NSDictionary *dic = @{@"error" : @"驾乘检索发送失败"};
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dic];
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        
    }else if ([type isEqualToString:@"drive"]){
        //自驾
        BMKDrivingRoutePlanOption *drivingRoutePlanOption = [[BMKDrivingRoutePlanOption alloc]init];
        drivingRoutePlanOption.from = startNode;
        drivingRoutePlanOption.to = endNode;
        switch ([policy intValue]) {
            case 0:
                drivingRoutePlanOption.drivingPolicy = BMK_DRIVING_FEE_FIRST;
                break;
            case 1:
                drivingRoutePlanOption.drivingPolicy = BMK_DRIVING_DIS_FIRST;
                break;
            case 2:
                drivingRoutePlanOption.drivingPolicy = BMK_DRIVING_TIME_FIRST;
                break;
            case 3:
                drivingRoutePlanOption.drivingPolicy = BMK_DRIVING_BLK_FIRST;
                break;
        }
        BOOL flag = [_rSearcher drivingSearch:drivingRoutePlanOption];
        if (!flag) {
            NSDictionary *dic = @{@"error" : @"驾乘检索发送失败"};
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dic];
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
    }else if ([type isEqualToString:@"transit"]){
        //乘车
        BMKTransitRoutePlanOption *transitRouteSearchOption = [[BMKTransitRoutePlanOption alloc]init];
        transitRouteSearchOption.from = startNode;
        transitRouteSearchOption.to = endNode;
        transitRouteSearchOption.city = [params objectForKey:@"sCityName"];
        //选择策略
        switch ([policy intValue]) {
            case 4:
                transitRouteSearchOption.transitPolicy = BMK_TRANSIT_NO_SUBWAY;
                break;
            case 5:
                transitRouteSearchOption.transitPolicy = BMK_TRANSIT_TIME_FIRST;
                break;
            case 6:
                transitRouteSearchOption.transitPolicy = BMK_TRANSIT_TRANSFER_FIRST;
                break;
            case 7:
                transitRouteSearchOption.transitPolicy = BMK_TRANSIT_WALK_FIRST;
        }
        BOOL flag = [_rSearcher transitSearch:transitRouteSearchOption];
        if (!flag) {
            NSDictionary *dic = @{@"error" : @"乘车检索发送失败"};
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dic];
            [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
        }
        
    }

}
//在地图上显示指定的路线
- (void)drawRoute:(CDVInvokedUrlCommand *)command{
    NSDictionary *params = [command argumentAtIndex:0];
    NSNumber *idNum = [params objectForKey:@"id"];
    _idNum = idNum;
    int type = 0;
    NSString *typeStr = [params objectForKey:@"type"];
    if ([typeStr isEqualToString:@"transit"]) {
        type = 2;
    }else {
        type = 4;
    }
    BOOL autoresizing = [[params objectForKey:@"autoresizing"] boolValue];
    //线路方案
    NSDictionary *routeDic = [params objectForKey:@"route"];
    //step点数组
    NSArray *stepsArray = [routeDic objectForKey:@"steps"];
    NSInteger size = stepsArray.count;
    int planPointCounts = 0;
    for (int i = 0; i<size; i++) {
        NSDictionary *transitStep = stepsArray[i];
        if (i == 0) {
            RouteAnnotation *item = [[RouteAnnotation alloc]init];
            CLLocationCoordinate2D coor;
            coor.longitude = [[[[routeDic objectForKey:@"terminal"] objectForKey:@"location"]objectForKey:@"longitude"] doubleValue];
            coor.latitude = [[[[routeDic objectForKey:@"terminal"] objectForKey:@"location"]objectForKey:@"latitude"] doubleValue];
            item.coordinate = coor;
            item.title = @"终点";
            item.type = 1;
            [_mapView addAnnotation:item];//添加起点标注
        }else if (i == size-1){
            RouteAnnotation *item = [[RouteAnnotation alloc]init];
            CLLocationCoordinate2D coor;
            coor.longitude = [[[[routeDic objectForKey:@"starting"] objectForKey:@"location"]objectForKey:@"longitude"] doubleValue];
            coor.latitude = [[[[routeDic objectForKey:@"starting"] objectForKey:@"location"]objectForKey:@"latitude"] doubleValue];
            item.coordinate = coor;
            item.title = @"起点";
            item.type = 0;
            [_mapView addAnnotation:item];//添加终点标注
        }
        //添加annotation节点
        RouteAnnotation *item = [[RouteAnnotation alloc]init];
        CLLocationCoordinate2D coor;
        coor.latitude = [[[[transitStep objectForKey:@"entrace"] objectForKey:@"location"] objectForKey:@"latitude"] doubleValue];
        coor.longitude = [[[[transitStep objectForKey:@"entrace"] objectForKey:@"location"] objectForKey:@"longitude"] doubleValue];
        item.coordinate = coor;
        item.title = [transitStep objectForKey:@"instruction"];
        item.type = type;
        item.degree = [[transitStep objectForKey:@"direction"] intValue]*30;
        [_mapView addAnnotation:item];
        //轨迹点总数累计
        planPointCounts += [[transitStep objectForKey:@"pointsCount"] intValue];
    }
    //轨迹点
    BMKMapPoint *tempPoints = new BMKMapPoint[planPointCounts];
    int i = 0;
    for (int j = 0; j<size; j++) {
        NSDictionary *transitStep = stepsArray[j];
        int k = 0;
        for (k = 0; k < [[transitStep objectForKey:@"pointsCount"] intValue]; k++) {
            NSArray * points = [transitStep objectForKey:@"points"];
            tempPoints[i].x = [[points[k] objectForKey:@"x"] doubleValue];
            tempPoints[i].y = [[points[k] objectForKey:@"y"] doubleValue];
            i++;
        }
    }
    //通过轨迹点构建路线图
    BMKPolyline *polyLine = [BMKPolyline polylineWithPoints:tempPoints count:planPointCounts];
    [_mapView addOverlay:polyLine];
    NSDictionary *overlayDic = @{@"id":idNum,@"overlay":polyLine};
    [_overlayArray addObject:overlayDic];
    delete []tempPoints;
    if (autoresizing) {
        [self mapViewFitPolyLine:polyLine];
    }
    
}
//移除指定id的路线图
- (void)removeRoute:(CDVInvokedUrlCommand *)command{
    NSArray *idArray = [command argumentAtIndex:0];
    
    for (NSNumber *idNum in idArray) {
        //首先移除折线
        for (NSDictionary *dic in _overlayArray) {
            if (idNum == [dic objectForKey:@"id"]) {
                [_mapView removeOverlay:[dic objectForKey:@"overlay"]];
            }
        }
        //移除添加的标注
        for (BMKPointAnnotation *point in [_mapView annotations]) {
            BMKAnnotationView *annoView = [_mapView viewForAnnotation:point];
            if (annoView.tag == [idNum intValue]) {
                [_mapView removeAnnotation:point];
            }
        }

    }
}
//搜索公交线路
- (void)searchBusRoute:(CDVInvokedUrlCommand *)command{
    _callBackId = command.callbackId;
    NSDictionary *params = [command argumentAtIndex:0];
    NSString *city = [params objectForKey:@"city"];
    NSString *line = [params objectForKey:@"line"];
    BMKCitySearchOption *citySearchOption = [[BMKCitySearchOption alloc]init];
    citySearchOption.city = city;
    _city = city;
    citySearchOption.keyword = line;
    BOOL flag = [_poiSearch poiSearchInCity:citySearchOption];
    if(flag)
    {
        NSLog(@"busline检索发送成功");
    }
    else
    {
        NSDictionary *dic = @{@"error" : @"busline检索发送失败"};
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dic];
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }

}
//根据 searchBusRoute 搜索返回的 uid 查询线路详情并绘制在地图上
- (void)drawBusRoute:(CDVInvokedUrlCommand *)command{
    NSDictionary *params = [command argumentAtIndex:0];
    NSNumber *idNum = [params objectForKey:@"id"];
    _idNum = idNum;
    BOOL autoresizing = [[params objectForKey:@"autoresizing"] boolValue];
    NSDictionary *busLineResult = [params objectForKey:@"busLine"];
    //站点信息
    NSArray *busStationsArray = [busLineResult objectForKey:@"busStations"];
    NSInteger size = 0;
    size = [busStationsArray count];
    for (NSInteger j = 0; j<size; j++) {
        RouteAnnotation *item = [[RouteAnnotation alloc]init];
        NSDictionary *locationDic = [busStationsArray[j] objectForKey:@"location"];
        CLLocationCoordinate2D coor;
        coor.latitude = [[locationDic objectForKey:@"latitude"] doubleValue];
        coor.longitude = [[locationDic objectForKey:@"longitude"] doubleValue];
        item.coordinate = coor;
        item.title = [busStationsArray[j] objectForKey:@"title"];
        item.type = 2;
        [_mapView addAnnotation:item];
    }
    //路段信息
    NSArray *busStepArray = [busLineResult objectForKey:@"busSteps"];
    NSInteger index = 0;
    //累加index为下面生命数组时使用
    for (NSInteger j = 0; j<busStepArray.count; j++) {
        NSDictionary *stepDic = busStepArray[j];
        index += [[stepDic objectForKey:@"pointsCount"] integerValue];
    }
    //直角坐标系划线
    BMKMapPoint *tempPoints = new BMKMapPoint[index];
    NSInteger k = 0;
    for (NSInteger i = 0; i<busStepArray.count; i++) {
        NSDictionary *stepDic = busStepArray[i];
        for (NSInteger j = 0; j<[[stepDic objectForKey:@"pointsCount"] integerValue]; j++) {
            NSArray *pointArray = [stepDic objectForKey:@"points"];
            tempPoints[k].x = [[pointArray[j] objectForKey:@"x"] doubleValue];
            tempPoints[k].y = [[pointArray[j] objectForKey:@"y"] doubleValue];
            k++;
        }
    }
    BMKPolyline* polyLine = [BMKPolyline polylineWithPoints:tempPoints count:index];
    [_mapView addOverlay:polyLine];
    NSDictionary *dic = @{@"id":idNum,@"overlay":polyLine};
    [_overlayArray addObject:dic];
    delete []tempPoints;
    NSDictionary *location = [[[busLineResult objectForKey:@"busStations"] objectAtIndex:0] objectForKey:@"location"];
    CLLocationCoordinate2D start;
    start.latitude = [[location objectForKey:@"latitude"] doubleValue];
    start.longitude = [[location objectForKey:@"longitude"] doubleValue];
    if (autoresizing) {
        [_mapView setCenterCoordinate:start animated:YES];
    }
}
//根据ID移除地图上显示的公交,地铁线路
- (void)removeBusRoute:(CDVInvokedUrlCommand *)command{
    
}
//根据单个关键字搜索兴趣点，无需调用 open 接口即可搜索
- (void)searchInCity:(CDVInvokedUrlCommand *)command{
    _callBackId = command.callbackId;
    NSDictionary *params = [command argumentAtIndex:0];
    NSString *city = [params objectForKey:@"city"];
    NSString *keyword = [params objectForKey:@"keyword"];
    int curPage = 0;
    BMKCitySearchOption *citySearchOption = [[BMKCitySearchOption alloc]init];
    citySearchOption.pageIndex = curPage;
    citySearchOption.pageCapacity = 50;
    citySearchOption.city= city;
    citySearchOption.keyword = keyword;
    BOOL flag = [_poiSearch poiSearchInCity:citySearchOption];
    if(flag)
    {
        NSLog(@"城市内检索发送成功");
    }
    else
    {
        NSDictionary *dic = @{@"error" : @"城市内检索发送失败"};
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dic];
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }

}
//根据单个关键字在圆形区域内搜索兴趣点，无需调用 open 接口即可搜索
- (void)searchNearby:(CDVInvokedUrlCommand *)command{
    _callBackId = command.callbackId;
    NSDictionary *params = [command argumentAtIndex:0];
    NSNumber *lonNum = [params objectForKey:@"lon"];
    NSNumber *latNum = [params objectForKey:@"lat"];
    NSNumber *radiusNum = [params objectForKey:@"radius"];
    NSString *keyword = [params objectForKey:@"keyword"];
    BMKNearbySearchOption *nearbySearchOption = [[BMKNearbySearchOption alloc]init];
    nearbySearchOption.pageIndex = 0;
    nearbySearchOption.pageCapacity = 50;
    nearbySearchOption.keyword = keyword;
    nearbySearchOption.radius = [radiusNum intValue];
    CLLocationCoordinate2D center;
    center.longitude = [lonNum floatValue];
    center.latitude = [latNum floatValue];
    nearbySearchOption.location = center;
    BOOL flag = [_poiSearch poiSearchNearBy:nearbySearchOption];
    if(flag)
    {
        NSLog(@"城市内检索发送成功");
    }
    else
    {
        NSDictionary *dic = @{@"error" : @"城市内检索发送失败"};
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dic];
        [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
    }
}
#pragma mark--BMKMapViewDelegate
- (BMKAnnotationView *)mapView:(BMKMapView *)view viewForAnnotation:(id <BMKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[RouteAnnotation class]]) {
        return [self getRouteAnnotationView:view viewForAnnotation:(RouteAnnotation*)annotation];
    }
    return nil;
}
//在地图上显示路线图
- (BMKAnnotationView*)getRouteAnnotationView:(BMKMapView *)mapview viewForAnnotation:(RouteAnnotation*)routeAnnotation
{
    BMKAnnotationView* view = nil;
    switch (routeAnnotation.type) {
        case 0:
        {
            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"start_node"];
            if (view == nil) {
                view = [[BMKAnnotationView alloc]initWithAnnotation:routeAnnotation reuseIdentifier:@"start_node"];
                view.image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/icon_nav_start.png"]];
                UIImage *image = view.image;
                view.centerOffset = CGPointMake(0, -(view.frame.size.height * 0.5));
                view.canShowCallout = TRUE;
            }
            view.annotation = routeAnnotation;
        }
            break;
        case 1:
        {
            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"end_node"];
            if (view == nil) {
                view = [[BMKAnnotationView alloc]initWithAnnotation:routeAnnotation reuseIdentifier:@"end_node"];
                view.image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/icon_nav_end.png"]];
                view.centerOffset = CGPointMake(0, -(view.frame.size.height * 0.5));
                view.canShowCallout = TRUE;
            }
            view.annotation = routeAnnotation;
        }
            break;
        case 2:
        {
            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"bus_node"];
            if (view == nil) {
                view = [[BMKAnnotationView alloc]initWithAnnotation:routeAnnotation reuseIdentifier:@"bus_node"];
                view.image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/icon_nav_bus.png"]];
                view.canShowCallout = TRUE;
            }
            view.annotation = routeAnnotation;
        }
            break;
        case 3:
        {
            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"rail_node"];
            if (view == nil) {
                view = [[BMKAnnotationView alloc]initWithAnnotation:routeAnnotation reuseIdentifier:@"rail_node"];
                view.image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/icon_nav_rail.png"]];
                view.canShowCallout = TRUE;
            }
            view.annotation = routeAnnotation;
        }
            break;
        case 4:
        {
            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"route_node"];
            if (view == nil) {
                view = [[BMKAnnotationView alloc]initWithAnnotation:routeAnnotation reuseIdentifier:@"route_node"];
                view.canShowCallout = TRUE;
            } else {
                [view setNeedsDisplay];
            }
            
            UIImage* image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/icon_direction.png"]];
            view.image = [image imageRotatedByDegrees:routeAnnotation.degree];
            view.annotation = routeAnnotation;
            
        }
            break;
        case 5:
        {
            view = [mapview dequeueReusableAnnotationViewWithIdentifier:@"waypoint_node"];
            if (view == nil) {
                view = [[BMKAnnotationView alloc]initWithAnnotation:routeAnnotation reuseIdentifier:@"waypoint_node"];
                view.canShowCallout = TRUE;
            } else {
                [view setNeedsDisplay];
            }
            
            UIImage* image = [UIImage imageWithContentsOfFile:[self getMyBundlePath1:@"images/icon_nav_waypoint.png"]];
            view.image = [image imageRotatedByDegrees:routeAnnotation.degree];
            view.annotation = routeAnnotation;
        }
            break;
        default:
            break;
    }
    view.tag = [_idNum integerValue];
    return view;
}


- (BMKOverlayView*)mapView:(BMKMapView *)map viewForOverlay:(id<BMKOverlay>)overlay
{
    if ([overlay isKindOfClass:[BMKPolyline class]]) {
        BMKPolylineView* polylineView = [[BMKPolylineView alloc] initWithOverlay:overlay];
        polylineView.fillColor = [[UIColor alloc] initWithRed:0 green:1 blue:1 alpha:1];
        polylineView.strokeColor = [[UIColor alloc] initWithRed:0 green:0 blue:1 alpha:0.7];
        polylineView.lineWidth = 3.0;
        return polylineView;
    }
    return nil;
}

#pragma mark--BMKRouteSearchDelegate
//乘车查询结果
-(void)onGetTransitRouteResult:(BMKRouteSearch *)searcher result:(BMKTransitRouteResult *)result errorCode:(BMKSearchErrorCode)error{
    if (error == BMK_SEARCH_NO_ERROR) {
        NSDictionary *resultDic = [self transTransitResultToDicitonnary:result];
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDic];
        [self.commandDelegate sendPluginResult:result callbackId:_callBackId];
    }else {
        NSDictionary *dic = @{@"error":[NSNumber numberWithInteger:error]};
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dic];
        [self.commandDelegate sendPluginResult:result callbackId:_callBackId];
    }
}
//驾车查询结果
-(void)onGetDrivingRouteResult:(BMKRouteSearch *)searcher result:(BMKDrivingRouteResult *)result errorCode:(BMKSearchErrorCode)error{
    if (error == BMK_SEARCH_NO_ERROR) {
        NSDictionary *resultDic = [self transDriveResultToDicitonnary:result];
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDic];
        [self.commandDelegate sendPluginResult:result callbackId:_callBackId];
        
    }else {
        //检索结果未正常返回
        NSDictionary *dic = @{@"error":[NSNumber numberWithInteger:error]};
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dic];
        [self.commandDelegate sendPluginResult:result callbackId:_callBackId];
    }
}
//步行查询结果
-(void)onGetWalkingRouteResult:(BMKRouteSearch *)searcher result:(BMKWalkingRouteResult *)result errorCode:(BMKSearchErrorCode)error{
    if (error == BMK_SEARCH_NO_ERROR) {
        NSDictionary *resultDic = [self transWalkResultToDicitonnary:(BMKWalkingRouteResult *)result];
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDic];
        [self.commandDelegate sendPluginResult:result callbackId:_callBackId];

    }else {
        NSDictionary *dic = @{@"error":[NSNumber numberWithInteger:error]};
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dic];
        [self.commandDelegate sendPluginResult:result callbackId:_callBackId];
    }
}
//公交线路查询结果
-(void)onGetBusDetailResult:(BMKBusLineSearch *)searcher result:(BMKBusLineResult *)busLineResult errorCode:(BMKSearchErrorCode)error{
    if (error == BMK_SEARCH_NO_ERROR) {
        //将结果转为标准的json格式
        NSDictionary *resultDic = [self transBusLineResultToDictionary:busLineResult];
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDic];
        [self.commandDelegate sendPluginResult:result callbackId:_callBackId];
    }else {
        NSDictionary *dic = @{@"error":[NSNumber numberWithInteger:error]};
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dic];
        [self.commandDelegate sendPluginResult:result callbackId:_callBackId];
    }
}

#pragma mark--BMKPoiSearchDelegate
- (void)onGetPoiResult:(BMKPoiSearch *)searcher result:(BMKPoiResult*)result errorCode:(BMKSearchErrorCode)error
{
    if (error == BMK_SEARCH_NO_ERROR) {
        BMKPoiInfo* poi = nil;
        BOOL findBusline = NO;
        _busPoiArray = [[NSMutableArray alloc]init];
        for (NSInteger i = 0; i < result.poiInfoList.count; i++) {
            poi = [result.poiInfoList objectAtIndex:i];
            //如果是公交地铁线路查询，则继续查询详情
            if (poi.epoitype == 2 || poi.epoitype == 4) {
                findBusline = YES;
                [_busPoiArray addObject:poi];
            }
        }
        //开始bueline详情搜索
        if(findBusline)
        {
            _currentIndex = 0;
            NSString* strKey = ((BMKPoiInfo*) [_busPoiArray objectAtIndex:_currentIndex]).uid;
            BMKBusLineSearchOption *buslineSearchOption = [[BMKBusLineSearchOption alloc]init];
            buslineSearchOption.city= _city;
            buslineSearchOption.busLineUid= strKey;
            BOOL flag = [_bSearcher busLineSearch:buslineSearchOption];
            if(flag)
            {
                NSLog(@"busline详情检索发送成功");
            }
            else
            {
                NSDictionary *dic = @{@"error" : @"busline详情检索发送失败"};
                CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dic];
                [self.commandDelegate sendPluginResult:result callbackId:_callBackId];
            }
        }else{
            //简单的查询
            NSDictionary *resultDic = [self transPoiResultToDictionary:result];
            CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:resultDic];
            [self.commandDelegate sendPluginResult:result callbackId:_callBackId];
        }
        
    }else {
        NSDictionary *dic = @{@"error":[NSNumber numberWithInteger:error]};
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:dic];
        [self.commandDelegate sendPluginResult:result callbackId:_callBackId];
    }
}


- (NSString*)getMyBundlePath1:(NSString *)filename
{
    
    NSBundle * libBundle = MYBUNDLE ;
    if ( libBundle && filename ){
        NSString * s=[[libBundle resourcePath ] stringByAppendingPathComponent : filename];
        return s;
    }
    return nil ;
}
//根据polyline设置地图范围
- (void)mapViewFitPolyLine:(BMKPolyline *) polyLine {
    CGFloat ltX, ltY, rbX, rbY;
    if (polyLine.pointCount < 1) {
        return;
    }
    BMKMapPoint pt = polyLine.points[0];
    ltX = pt.x, ltY = pt.y;
    rbX = pt.x, rbY = pt.y;
    for (int i = 1; i < polyLine.pointCount; i++) {
        BMKMapPoint pt = polyLine.points[i];
        if (pt.x < ltX) {
            ltX = pt.x;
        }
        if (pt.x > rbX) {
            rbX = pt.x;
        }
        if (pt.y > ltY) {
            ltY = pt.y;
        }
        if (pt.y < rbY) {
            rbY = pt.y;
        }
    }
    BMKMapRect rect;
    rect.origin = BMKMapPointMake(ltX , ltY);
    rect.size = BMKMapSizeMake(rbX - ltX, rbY - ltY);
    [_mapView setVisibleMapRect:rect];
    _mapView.zoomLevel = _mapView.zoomLevel - 0.3;
}

#pragma mark--搜索结果--->json
//将乘车查询结果转为标准的json（乘车）
- (NSDictionary *)transTransitResultToDicitonnary:(BMKTransitRouteResult *)result{
    NSDictionary *taxiInfo = [result.taxiInfo objectDictionary];
    NSMutableArray *routesArray = [[NSMutableArray alloc]init];
    NSDictionary *suggest = @{};
    if (!taxiInfo) {
        taxiInfo = @{};
    }
    NSDictionary *routeLineDic = [[NSDictionary alloc]init];
    NSDictionary *sLocationDic = [[NSDictionary alloc]init];
    NSDictionary *tLocationDic = [[NSDictionary alloc]init];
    NSDictionary *startDic = [[NSDictionary alloc]init];
    NSDictionary *termDic = [[NSDictionary alloc]init];
    NSDictionary *timeDic = [[NSDictionary alloc]init];
    NSDictionary *resultDic = [[NSDictionary alloc]init];
    for (BMKTransitRouteLine *line in result.routes) {
        NSMutableArray *stepArray = [[NSMutableArray alloc]init];
        for (BMKTransitStep *transitStep in line.steps) {
            NSDictionary *entrace = [[NSDictionary alloc]init];
            NSDictionary *enLocationDic = @{@"latitude":[NSNumber numberWithDouble:transitStep.entrace.location.latitude],@"longitude":[NSNumber numberWithDouble:transitStep.entrace.location.longitude]};
            if (!transitStep.entrace.title) {
                transitStep.entrace.title = @"";
            }
            if (!transitStep.entrace.uid) {
                transitStep.entrace.uid = @"";
            }
            if (!transitStep.exit.title) {
                transitStep.exit.title = @"";
            }
            if (!transitStep.exit.uid) {
                transitStep.exit.uid = @"";
            }
            
            entrace = @{@"uid":transitStep.entrace.uid,@"title":transitStep.entrace.title,@"location":enLocationDic};
            NSDictionary *exit = [[NSDictionary alloc]init];
            NSDictionary *exLocationDic = @{@"latitude":[NSNumber numberWithDouble:transitStep.exit.location.latitude],@"longitude":[NSNumber numberWithDouble:transitStep.exit.location.longitude]};
            exit = @{@"uid":transitStep.exit.uid,@"title":transitStep.exit.title,@"location":exLocationDic};
            NSString *instruction = transitStep.instruction;
            NSNumber *stepType = [NSNumber numberWithInt:transitStep.stepType];
            NSDictionary *vehicleInfo = [[NSDictionary alloc]init];
            if (transitStep.vehicleInfo) {
                if (!transitStep.vehicleInfo.uid) {
                    transitStep.vehicleInfo.uid = @"";
                }
                if (!transitStep.vehicleInfo.title) {
                    transitStep.vehicleInfo.title = @"";
                }
                vehicleInfo = @{@"uid":transitStep.vehicleInfo.uid,@"title":transitStep.vehicleInfo.title,@"passStationNum":[NSNumber numberWithInt:transitStep.vehicleInfo.passStationNum],@"totalPrice":[NSNumber numberWithInt:transitStep.vehicleInfo.totalPrice],@"zonePrice":[NSNumber numberWithInt:transitStep.vehicleInfo.zonePrice]};
            }else{
                vehicleInfo = @{};
            }
            NSMutableArray *points = [[NSMutableArray alloc]init];
            for (int i = 0; i<transitStep.pointsCount; i++) {
                double x = transitStep.points[i].x;
                double y = transitStep.points[i].y;
                NSDictionary *pointDic = @{@"x":[NSNumber numberWithDouble:x],@"y":[NSNumber numberWithDouble:y]};
                [points addObject:pointDic];
            }
            NSDictionary *transitStepDic = @{@"distance":[NSNumber numberWithInt:transitStep.distance],@"duration":[NSNumber numberWithInt:transitStep.duration],@"points":points,@"pointsCount":[NSNumber numberWithInt:transitStep.pointsCount],@"entrace":entrace,@"exit":exit,@"instruction":instruction,@"stepType":stepType,@"vehicleInfo":vehicleInfo};
            [stepArray addObject:transitStepDic];
            
        }
        BMKRouteNode *startNode = line.starting;
        sLocationDic = @{@"latitude":[NSNumber numberWithDouble:startNode.location.latitude],@"longitude":[NSNumber numberWithDouble:startNode.location.longitude]};
        startDic = @{@"uid":startNode.uid,@"title":startNode.title,@"location":sLocationDic};
        BMKRouteNode *termNode = line.terminal;
        tLocationDic = @{@"latitude":[NSNumber numberWithDouble:termNode.location.latitude],@"longitude":[NSNumber numberWithDouble:termNode.location.longitude]};
        termDic = @{@"uid":termNode.uid,@"title":termNode.title,@"location":tLocationDic};
        timeDic = @{@"dates":[NSNumber numberWithInt:line.duration.dates],@"hours":[NSNumber numberWithInt:line.duration.hours],@"minutes":[NSNumber numberWithInt:line.duration.minutes],@"seconds":[NSNumber numberWithInt:line.duration.seconds]};
        if (!line.title) {
            line.title = @"";
        }
        routeLineDic = @{@"distance":[NSNumber numberWithInt:line.distance],@"duration":timeDic,@"starting":startDic,@"terminal":termDic,@"title":line.title,@"steps":stepArray};
        [routesArray addObject:routeLineDic];
        
    }
    resultDic = @{@"taxiInfo":taxiInfo,@"suggestAddrResult":suggest,@"routes":routesArray};
    return resultDic;
}
//将自驾查询结果转换为标准的json
-(NSDictionary *)transDriveResultToDicitonnary:(BMKDrivingRouteResult *)result{
    NSDictionary *taxiInfo = [result.taxiInfo objectDictionary];
    if (!taxiInfo) {
        taxiInfo = @{};
    }
    NSDictionary * suggestAddrResult = @{};
    NSMutableArray *lineArray = [[NSMutableArray alloc]init];
    for (BMKDrivingRouteLine *drivingRouteLine in result.routes) {
        NSMutableArray *stepArray = [[NSMutableArray alloc]init];
        for (BMKDrivingStep *routeStep in drivingRouteLine.steps) {
            NSNumber *distance = [NSNumber numberWithInt:routeStep.distance];
            NSNumber *duration = [NSNumber numberWithInt:routeStep.duration];
            NSNumber *pointsCount = [NSNumber numberWithInt:routeStep.pointsCount];
            NSNumber *direction = [NSNumber numberWithInt:routeStep.direction];
            if (!routeStep.entrace.uid) {
                routeStep.entrace.uid = @"";
            }
            if (!routeStep.entrace.title) {
                routeStep.entrace.title = @"";
            }
            if (!routeStep.exit.uid) {
                routeStep.exit.uid = @"";
            }
            if (!routeStep.exit.title) {
                routeStep.exit.title = @"";
            }
            NSDictionary *enLocationDic = @{@"latitude":[NSNumber numberWithDouble:routeStep.entrace.location.latitude],@"longitude":[NSNumber numberWithDouble:routeStep.entrace.location.longitude]};
            NSDictionary *entrace = @{@"uid":routeStep.entrace.uid,@"title":routeStep.entrace.title,@"location":enLocationDic};
            NSString *entaceInstruction = routeStep.entraceInstruction;
            NSDictionary *exLocationDic = @{@"latitude":[NSNumber numberWithDouble:routeStep.exit.location.latitude],@"longitude":[NSNumber numberWithDouble:routeStep.exit.location.longitude]};
            NSDictionary *exit = @{@"uid":routeStep.exit.uid,@"title":routeStep.exit.title,@"location":exLocationDic};
            NSString *exitInstruction = routeStep.exitInstruction;
            NSString *instruction = routeStep.instruction;
            NSNumber *numTurns = [NSNumber numberWithInt:routeStep.numTurns];
            NSNumber *hasTrafficInfo = [NSNumber numberWithBool:routeStep.hasTrafficsInfo];
            NSArray *traffics = @[];
            NSMutableArray *points = [[NSMutableArray alloc]init];
            for (int i = 0; i<routeStep.pointsCount; i++) {
                double x = routeStep.points[i].x;
                double y = routeStep.points[i].y;
                NSDictionary *pointDic = @{@"x":[NSNumber numberWithDouble:x],@"y":[NSNumber numberWithDouble:y]};
                [points addObject:pointDic];
            }

            NSDictionary *stepDic = @{@"distance":distance,@"duration":duration,@"points":points,@"pointsCount":pointsCount,@"direction":direction,@"entrace":entrace,@"entraceInstruction":entaceInstruction,@"exit":exit,@"exitInstruction":exitInstruction,@"instruction":instruction,@"numTurns":numTurns,@"numTurns":hasTrafficInfo,@"traffics":traffics};
            [stepArray addObject:stepDic];
        }
        NSNumber *distance = [NSNumber numberWithInt:drivingRouteLine.distance];
        NSDictionary *duration = [drivingRouteLine.duration objectDictionary];
        if (!drivingRouteLine.starting.title) {
            drivingRouteLine.starting.title = @"";
        }
        if (!drivingRouteLine.starting.uid) {
            drivingRouteLine.starting.uid = @"";
        }
        if (!drivingRouteLine.terminal.title) {
            drivingRouteLine.terminal.title = @"";
        }
        if (!drivingRouteLine.terminal.uid) {
            drivingRouteLine.terminal.uid = @"";
        }
        NSDictionary *sLocationDic = @{@"latitude":[NSNumber numberWithDouble:drivingRouteLine.starting.location.latitude],@"longitude":[NSNumber numberWithDouble:drivingRouteLine.starting.location.longitude]};
        NSDictionary *starting = @{@"uid":drivingRouteLine.starting.uid,@"title":drivingRouteLine.starting.title,@"location":sLocationDic};
        NSDictionary *tLocationDic = @{@"latitude":[NSNumber numberWithDouble:drivingRouteLine.terminal.location.latitude],@"longitude":[NSNumber numberWithDouble:drivingRouteLine.terminal.location.longitude]};
        NSDictionary *terminal = @{@"uid":drivingRouteLine.terminal.uid,@"title":drivingRouteLine.terminal.title,@"location":tLocationDic};
        if (!drivingRouteLine.title) {
            drivingRouteLine.title = @"";
        }
        NSString *title = drivingRouteLine.title;
        NSNumber *isSupportTraffic = [NSNumber numberWithBool:drivingRouteLine.isSupportTraffic];
        NSMutableArray *wayPoints = [[NSMutableArray alloc]init];
        for (BMKPlanNode *planNode in drivingRouteLine.wayPoints) {
            NSString *cityName = planNode.cityName;
            NSString *name = planNode.name;
            NSDictionary *pt = @{@"latitude":[NSNumber numberWithDouble:planNode.pt.latitude],@"longitude":[NSNumber numberWithDouble:planNode.pt.longitude]};
            NSDictionary *planNodeDic = @{@"cityName":cityName,@"name":name,@"pt":pt};
            [wayPoints addObject:planNodeDic];
        }
        NSDictionary *routeLineDic = @{@"distance":distance,@"duration":duration,@"starting":starting,@"terminal":terminal,@"title":title,@"steps":stepArray,@"isSupportTraffic":isSupportTraffic,@"wayPoints":wayPoints};
        [lineArray addObject:routeLineDic];
    }
    NSDictionary *resultDic = @{@"taxiInfo":taxiInfo,@"suggestAddrResult":suggestAddrResult,@"routes":lineArray};
    return resultDic;
    
}
//将步行结果转换为标准的json
-(NSDictionary *)transWalkResultToDicitonnary:(BMKWalkingRouteResult *)result{
    NSDictionary *taxiInfo = [result.taxiInfo objectDictionary];
    if (!taxiInfo) {
        taxiInfo = @{};
    }
    NSDictionary *suggestAddrResult = @{};
    NSMutableArray *lineArray = [[NSMutableArray alloc]init];
    for (BMKWalkingRouteLine *walkingLine in result.routes) {
        NSMutableArray *stepArray = [[NSMutableArray alloc]init];
        for (BMKWalkingStep *routeStep in walkingLine.steps) {
            NSNumber *distance = [NSNumber numberWithInt:routeStep.distance];
            NSNumber *duration = [NSNumber numberWithInt:routeStep.duration];
            NSNumber *pointsCount = [NSNumber numberWithInt:routeStep.pointsCount];
            NSNumber *direction = [NSNumber numberWithInt:routeStep.direction];
            if (!routeStep.entrace.uid) {
                routeStep.entrace.uid = @"";
            }
            if (!routeStep.entrace.title) {
                routeStep.entrace.title = @"";
            }
            if (!routeStep.exit.uid) {
                routeStep.exit.uid = @"";
            }
            if (!routeStep.exit.title) {
                routeStep.exit.title = @"";
            }
            NSDictionary *enLocationDic = @{@"latitude":[NSNumber numberWithDouble:routeStep.entrace.location.latitude],@"longitude":[NSNumber numberWithDouble:routeStep.entrace.location.longitude]};
            NSDictionary *entrace = @{@"uid":routeStep.entrace.uid,@"title":routeStep.entrace.title,@"location":enLocationDic};
            NSString *entaceInstruction = routeStep.entraceInstruction;
            NSDictionary *exLocationDic = @{@"latitude":[NSNumber numberWithDouble:routeStep.exit.location.latitude],@"longitude":[NSNumber numberWithDouble:routeStep.exit.location.longitude]};
            NSDictionary *exit = @{@"uid":routeStep.exit.uid,@"title":routeStep.exit.title,@"location":exLocationDic};
            NSString *exitInstruction = routeStep.exitInstruction;
            NSString *instruction = routeStep.instruction;
            NSMutableArray *points = [[NSMutableArray alloc]init];
            for (int i = 0; i<routeStep.pointsCount; i++) {
                double x = routeStep.points[i].x;
                double y = routeStep.points[i].y;
                NSDictionary *pointDic = @{@"x":[NSNumber numberWithDouble:x],@"y":[NSNumber numberWithDouble:y]};
                [points addObject:pointDic];
            }
            NSDictionary *stepDic = @{@"distance":distance,@"duration":duration,@"points":points,@"pointsCount":pointsCount,@"direction":direction,@"entrace":entrace,@"entaceInstruction":entaceInstruction,@"exit":exit,@"exitInstruction":exitInstruction,@"instruction":instruction};
            [stepArray addObject:stepDic];
        }
        NSNumber *distance = [NSNumber numberWithInt:walkingLine.distance];
        NSDictionary *duration = [walkingLine.duration objectDictionary];
        if (!walkingLine.starting.title) {
            walkingLine.starting.title = @"";
        }
        if (!walkingLine.starting.uid) {
            walkingLine.starting.uid = @"";
        }
        if (!walkingLine.terminal.title) {
            walkingLine.terminal.title = @"";
        }
        if (!walkingLine.terminal.uid) {
            walkingLine.terminal.uid = @"";
        }
        NSDictionary *sLocationDic = @{@"latitude":[NSNumber numberWithDouble:walkingLine.starting.location.latitude],@"longitude":[NSNumber numberWithDouble:walkingLine.starting.location.longitude]};
        NSDictionary *starting = @{@"uid":walkingLine.starting.uid,@"title":walkingLine.starting.title,@"location":sLocationDic};
        NSDictionary *tLocationDic = @{@"latitude":[NSNumber numberWithDouble:walkingLine.terminal.location.latitude],@"longitude":[NSNumber numberWithDouble:walkingLine.terminal.location.longitude]};
        NSDictionary *terminal = @{@"uid":walkingLine.terminal.uid,@"title":walkingLine.terminal.title,@"location":tLocationDic};
        if (!walkingLine.title) {
            walkingLine.title = @"";
        }
        NSString *title = walkingLine.title;
        NSDictionary *routeLineDic = @{@"distance":distance,@"duration":duration,@"starting":starting,@"terminal":terminal,@"title":title,@"steps":stepArray};
        [lineArray addObject:routeLineDic];
    }
    NSDictionary *resultDic = @{@"taxiInfo":taxiInfo,@"suggestAddrResult":suggestAddrResult,@"routes":lineArray};
    return resultDic;
    
}
//将公交线路查询结果转为标准的json
-(NSDictionary *)transBusLineResultToDictionary:(BMKBusLineResult *)busLineResult{
    NSMutableArray *busStationsArray = [[NSMutableArray alloc]init];
    NSMutableArray *busStepsArray = [[NSMutableArray alloc]init];
    for (BMKBusStation *busStation in busLineResult.busStations) {
        CLLocationCoordinate2D location = busStation.location;
        NSDictionary *locationDic = @{@"latitude":[NSNumber numberWithDouble:location.latitude],@"longitude":[NSNumber numberWithDouble:location.longitude]};
        NSDictionary *nodeDic = @{@"uid":busStation.uid,@"title":busStation.title,@"location":locationDic};
        [busStationsArray addObject:nodeDic];
    }
    for (BMKBusStep *busStep in busLineResult.busSteps) {
        NSMutableArray *points = [[NSMutableArray alloc]init];
        for (int i = 0; i<busStep.pointsCount; i++) {
            double x = busStep.points[i].x;
            double y = busStep.points[i].y;
            NSDictionary *pointDic = @{@"x":[NSNumber numberWithDouble:x],@"y":[NSNumber numberWithDouble:y]};
            [points addObject:pointDic];
        }

        NSDictionary *stepDic = @{@"distance":[NSNumber numberWithInt:busStep.distance],@"duration":[NSNumber numberWithInt:busStep.duration],@"points":points,@"pointsCount":[NSNumber numberWithInt:busStep.pointsCount]};
        [busStepsArray addObject:stepDic];
    }
    NSDictionary *resultDic = @{@"busCompany":busLineResult.busCompany,@"busLineName":busLineResult.busLineName,@"uid":busLineResult.uid,@"startTime":busLineResult.startTime,@"endTime":busLineResult.endTime,@"isMonTicket":[NSNumber numberWithInt:busLineResult.isMonTicket],@"busStations":busStationsArray,@"busSteps":busStepsArray};
    return resultDic;
}
//将兴趣点查询结果转换为标准的json
-(NSDictionary *)transPoiResultToDictionary:(BMKPoiResult *)result{
    NSMutableArray *pointInfoList = [[NSMutableArray alloc]init];
    NSMutableArray *cityListArray = [[NSMutableArray alloc]init];
    for (BMKPoiInfo *poiInfo in result.poiInfoList) {
        CLLocationCoordinate2D pt = poiInfo.pt;
        NSDictionary *ptDic = @{@"latitude":[NSNumber numberWithDouble:pt.latitude],@"longitude":[NSNumber numberWithDouble:pt.longitude]};
        //为保证严谨，在此参数过滤
        if (!ptDic) {
            ptDic = @{};
        }
        if (!poiInfo.name) {
            poiInfo.name = @"";
        }
        if (!poiInfo.epoitype) {
            poiInfo.epoitype = 0;
        }
        if (!poiInfo.address) {
            poiInfo.address = @"";
        }
        if (!poiInfo.city) {
            poiInfo.city = @"";
        }
        if (!poiInfo.phone) {
            poiInfo.phone = @"";
        }
        if (!poiInfo.uid) {
            poiInfo.uid = @"";
        }
        NSDictionary *poiInfoListDic = @{@"name":poiInfo.name,@"uid":poiInfo.uid,@"address":poiInfo.address,@"city":poiInfo.city,@"phone":poiInfo.phone,@"epoitype":[NSNumber numberWithInt:poiInfo.epoitype],@"pt":ptDic};
        
        [pointInfoList addObject:poiInfoListDic];
    }
    for (BMKCityListInfo *cityListInfo in result.cityList) {
        NSDictionary *cityInfoListDic = @{@"city":cityListInfo.city,@"num":[NSNumber numberWithInt:cityListInfo.num]};
        [cityListArray addObject:cityInfoListDic];
    }
    NSDictionary *resultDic = @{@"totalPoiNum":[NSNumber numberWithInt:result.totalPoiNum],@"currPoiNum":[NSNumber numberWithInt:result.currPoiNum],@"pageNum":[NSNumber numberWithInt:result.pageNum],@"pageIndex":[NSNumber numberWithInt:result.pageIndex],@"poiInfoList":pointInfoList,@"cityList":cityListArray};
    return resultDic;
}
-(void)dealloc{
    _poiSearch.delegate = nil;
    _bSearcher.delegate = nil;
    _rSearcher.delegate = nil;
}


@end
