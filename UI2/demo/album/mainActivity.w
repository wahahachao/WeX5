<?xml version="1.0" encoding="UTF-8"?>

<div xmlns="http://www.w3.org/1999/xhtml" xid="window" class="window" component="$UI/system/components/justep/window/window" design="device:mobile">  
  <div component="$UI/system/components/justep/model/model" xid="model" onLoad="modelLoad" style="height:auto;left:165px;top:18px;"><div component="$UI/system/components/justep/data/data" autoLoad="true" xid="picData" idColumn="picSrc"><column label="图片地址" name="picSrc" type="String" xid="xid1"></column>
  <data xid="default1">[{&quot;picSrc&quot;:&quot;$UI/demo/picList/images/1.jpg&quot;},{&quot;picSrc&quot;:&quot;$UI/demo/picList/images/2.jpg&quot;},{&quot;picSrc&quot;:&quot;$UI/demo/picList/images/3.jpg&quot;},{&quot;picSrc&quot;:&quot;$UI/demo/picList/images/4.jpg&quot;}]</data></div></div> 
<div component="$UI/system/components/justep/panel/panel" class="x-panel x-full" xid="panel1">
   <div class="x-panel-top" xid="top1"><div component="$UI/system/components/justep/titleBar/titleBar" class="x-titlebar" xid="titleBar1" title="相册">
   <div class="x-titlebar-left" xid="div1"><a component="$UI/system/components/justep/button/button" class="btn btn-link btn-only-icon" xid="button2" icon="icon-chevron-left" onClick='{"operation":"window.close"}'>
   <i xid="i3" class="icon-chevron-left"></i>
   <span xid="span2"></span></a></div>
   <div class="x-titlebar-title" xid="div2">相册</div>
   <div class="x-titlebar-right reverse" xid="div3"><a component="$UI/system/components/justep/button/button" class="btn btn-link btn-only-icon" label="button" xid="button1" icon="icon-refresh">
   <i xid="i1" class="icon-refresh"></i>
   <span xid="span1"></span></a></div></div></div>
   <div class="x-panel-content" xid="content1"><div component="$UI/system/components/justep/list/list" class="x-list" xid="list1" data="picData">
   <ul class="x-list-template" xid="listTemplateUl1" bind-click="listTemplateUl1Click">
    <li xid="li1" class="col col-xs-3 col-sm-3 col-md-6 col-lg-6"><img src="" alt="" xid="image" bind-attr-src="$model.getImageUrl($object)" style="width:100%;" height="auto" class="img-responsive img-thumbnail"></img></li></ul> </div></div>
   <div class="x-panel-bottom" xid="bottom1"></div></div></div>